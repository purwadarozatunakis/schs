<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAbsensiMurid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::table('t_absensi', function (Blueprint $table) {
            $table->string('kode_semster')->nullable();
            $table->string('tahun_ajaran')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        Schema::table('t_absensi', function (Blueprint $table) {
            $table->dropColumn('kode_smester');
            $table->dropColumn('tahun_ajaran');
        });
    }
}
