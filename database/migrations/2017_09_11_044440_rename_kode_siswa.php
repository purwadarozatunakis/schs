<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameKodeSiswa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

        Schema::table('t_absensi', function (Blueprint $table) {
            $table->renameColumn('kode_siswa', 'kode_murid');
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('t_absensi', function (Blueprint $table) {
            $table->renameColumn('kode_murid', 'kode_siswa');
        });
    }
}
