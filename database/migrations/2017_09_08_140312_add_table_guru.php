<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableGuru extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('t_guru', function (Blueprint $table) {
            $table->string('nip')->index();
            $table->string('nama');
            $table->string('email');
            $table->string('no_tlp');
            $table->timestamps();
        });

        Schema::create('t_mapel', function (Blueprint $table) {
            $table->string('kode_mapel');
            $table->string('nama');
            $table->string('kode_guru');
            $table->string('type_mapel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::create('t_guru');
        Schema::create('t_mapel');
    }
}
