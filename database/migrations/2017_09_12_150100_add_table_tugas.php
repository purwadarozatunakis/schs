<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableTugas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('t_tugas', function (Blueprint $table) {
            $table->string('kode_tugas')->index();
            $table->string('kode_kelas');
            $table->string('kode_mapel');
            $table->string('kode_guru');
            $table->string('jenis_tugas');
            $table->date('tanggal_pengumpulan');
            $table->text('soal_tugas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('t_tugas');
    }
}
