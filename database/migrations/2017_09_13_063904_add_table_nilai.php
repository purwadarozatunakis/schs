<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableNilai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('t_nilai_tugas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_murid');
            $table->string('kode_mapel');
            $table->string('kode_tugas');
            $table->string('kode_smester');
            $table->string('tahun_ajaran');
            $table->double('nilai');
            $table->text('keterangan')->nullable;
            $table->timestamps();
        });

        Schema::create('t_nilai', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_murid');
            $table->string('kode_mapel');
            $table->string('kode_smester');
            $table->string('tahun_ajaran');
            $table->double('nilai_uts');
            $table->double('nilai_uas');
            $table->text('keterangan')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('t_nilai');
        Schema::drop('t_nilai_tugas');
    }
}
