<?php

use Illuminate\Database\Seeder;
use App\Models\Jurusan;

class JurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $jurusan = [
            ['MEKA' => 'Mekatronika'],
            ['RPL' => 'Rekayasa Perangkat Lunak'],
            ['MULTI' => 'Multimedia'],
            ['ANIMASI' => 'Animasi'],
            ['KI' => 'Kimia Industri'],
            ['TP' => 'Teknik Permesinan'],
        ];

        foreach ($jurusan as $key => $value) {
            Jurusan::create([
                ''
            ]);
        }
    }
}
