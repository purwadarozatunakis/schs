# Semantic Form
[![SensioLabsInsight](https://img.shields.io/sensiolabs/i/7378998a-4d74-43aa-841a-d85b74579734.svg)](https://insight.sensiolabs.com/projects/7378998a-4d74-43aa-841a-d85b74579734)
[![Travis](https://img.shields.io/travis/laravolt/boostrap4-mod.svg)](https://travis-ci.org/laravolt/boostrap4-mod)
[![Coverage Status](https://coveralls.io/repos/github/laravolt/boostrap4-mod/badge.svg?branch=master)](https://coveralls.io/github/laravolt/boostrap4-mod?branch=master)

[Semantic UI](http://semantic-ui.com/) form builder, for Laravel.

## Installation

Via Composer

``` bash
$ composer require laravolt/boostrap4-mod
```

## Service Provider
``` php
Sagalagarap\Boostrap3Mod\ServiceProvider::class,
```

## Facade (Alias)
``` php
'Boostrap3Mod'    => Sagalagarap\Boostrap3Mod\Facade::class,
```

## API

### Opening Form
``` php
Boostrap3Mod::open('search'); // action="search"
Boostrap3Mod::open()->get();
Boostrap3Mod::open()->post();
Boostrap3Mod::open()->put();
Boostrap3Mod::open()->patch();
Boostrap3Mod::open()->delete();
Boostrap3Mod::open(); // default to method="GET"
Boostrap3Mod::open()->action('search');
Boostrap3Mod::open()->url('search'); // alias for action()
Boostrap3Mod::open()->route('route.name');
Boostrap3Mod::open()->post()->action(route('comment.store'));
```

### Opening Form (Short Syntax, Since 1.10)
``` php
Boostrap3Mod::open('search'); // action="search" method=POST
Boostrap3Mod::get('search'); // action="search" method=GET
Boostrap3Mod::post('search'); // action="search" method=POST
Boostrap3Mod::put('search'); // action="search" method=POST _method=PUT
Boostrap3Mod::patch('search'); // action="search" method=POST _method=PATCH
Boostrap3Mod::delete('search'); // action="search" method=POST _method=DELETE
```

### Input Text
``` php
Boostrap3Mod::text($name, $value)->label('Username');
```

### Password
``` php
Boostrap3Mod::password($name)->label('Password');
```

### Email
``` php
Boostrap3Mod::email($name, $value)->label('Email Address');
```
### Textarea
``` php
Boostrap3Mod::textarea($name, $value)->label('Note');
```

### Select Box (Dropdown)
``` php
Boostrap3Mod::select($name, $options)->label('Choose Country');
Boostrap3Mod::select($name, $options, $selected)->label('Choose Country');
Boostrap3Mod::select($name, $options)->placeholder('--Select--');
Boostrap3Mod::select($name, $options)->appendOption($key, $label);
Boostrap3Mod::select($name, $options)->prependOption($key, $label);
```

### Select Date & Date Time
``` php
Boostrap3Mod::selectDate('myDate', $startYear, $endYear)->label('Birth Date');
Boostrap3Mod::selectDateTime('myDate', $startYear, $endYear, $intervalInMinute)->label('Schedule');
```

By default, selectDate and selectDateTime will post request as `_myDate` with `['date'=>4, 'month'=>5, 'year'=>2016]` for example.
To get `2016-5-4` format, you need to register middleware and use it in the routes.

```php
protected $routeMiddleware = [
    'selectdate' => \Sagalagarap\Boostrap3Mod\Middleware\SelectDateMiddleware::class,
    'selectdatetime' => \Sagalagarap\Boostrap3Mod\Middleware\SelectDateTimeMiddleware::class
];
```

```php
Route::post('myForm', ['middleware' => ['web', 'selectdate:myDate'], function (\Illuminate\Http\Request $request) {
	dd($request->input('myDate')); // Will output 2016-5-4
}]);
```

### Select Range
``` php
Boostrap3Mod::selectRange($name, $begin, $end)->label('Number of child');
```

### Select Month
``` php
Boostrap3Mod::selectMonth($name, $format = '%B')->label('Month');
```

### Radio
``` php
$checked = true;
Boostrap3Mod::radio($name, $value, $checked)->label('Item Label');
```

### Radio Group
``` php
$values = ['apple' => 'Apple', 'banana' => 'Banana'];
$checkedValue = 'banana';
Boostrap3Mod::radioGroup($name, $values, $checkedValue)->label('Select Fruit');
```

### Checkbox
``` php
Boostrap3Mod::checkbox($name, $value, $checked)->label('Remember Me');
```

### Checkbox Group
``` php
$values = ['apple' => 'Apple', 'banana' => 'Banana'];
$checkedValue = 'banana';
Boostrap3Mod::checkboxGroup($name, $values, $checkedValue)->label('Select Fruit');
```

### File
``` php
Boostrap3Mod::file($name);
```
### Input Wrapper
``` php
Boostrap3Mod::input($name, $defaultvalue);
Boostrap3Mod::input($name, $defaultvalue)->appendIcon('search');
Boostrap3Mod::input($name, $defaultvalue)->prependIcon('users');
Boostrap3Mod::input($name, $defaultvalue)->appendLabel($label);
Boostrap3Mod::input($name, $defaultvalue)->prependLabel($label);
Boostrap3Mod::input($name, $defaultvalue)->type("password");
```
Reference: http://semantic-ui.com/elements/input.html

### Image (Not Yet Implemented)
``` php
Boostrap3Mod::image($name);
```

### Datepicker (experimental)
``` php
// somewhere in view
Boostrap3Mod::datepicker($name, $value, $format);

// don't forget to put this somewhere on your view
@include('boostrap4-mod::scripts.calendar')

// Valid $format are:
// DD -> two digit date
// MM -> two digit month number
// MMMM -> month name (localized)
// YY -> two digit year
// YYYY -> full year

// To convert localized format to standard (SQL) datetime format, you can use Jenssegers\Date\Date library (already included):
// Jenssegers\Date\Date::createFromFormat('d F Y', '12 februari 2000')->startOfDay()->toDateTimeString();
// Jenssegers\Date\Date::createFromFormat('d F Y', '12 februari 2000')->startOfDay()->toDateString();
```
See https://github.com/Semantic-Org/Semantic-UI/pull/3256 for further discussion. Remember, you must include calendar.js and calendar.css on your own.

### Redactor (Not Yet Implemented)
``` php
Boostrap3Mod::redactor($name, $value)->label('Post Body');
```

### Hidden
``` php
Boostrap3Mod::hidden($name, $value);
```

### Button
``` php
Boostrap3Mod::button($value);
```

### Submit
``` php
Boostrap3Mod::submit($value);
```

### Model Binding
``` php
Boostrap3Mod::bind($model);
```

### General Function
For every form element, you can call and chaining following methods:

* id($string)
* addClass($string)
* removeClass($string)
* attribute($name, $value)
* data($name, $value)
* hint($text) (Since 1.10.0)
* hint($text, $class = "hint") (Since 1.10.2)

#### Override Hint class globally (Since 1.10.2)
``` php
// Put this on every request, e.g. in AppServiceProvider
Sagalagarap\Boostrap3Mod\Elements\Hint::$defaultClass = 'custom-class';
```

#### Example
``` php
Boostrap3Mod::text($name, $value)->label('Username')->id('username')->addClass('foo');
Boostrap3Mod::text($name, $value)->label('Username')->data('url', 'http://id-laravel.com');
Boostrap3Mod::password($name, $value)->label('Password')->hint('Minimum 6 characters');
Boostrap3Mod::password($name, $value)->label('Password')->hint('Minimum 6 characters', 'my-custom-css-class');
```

### Middleware

* \Sagalagarap\Boostrap3Mod\Middleware\SelectDateMiddleware
* \Sagalagarap\Boostrap3Mod\Middleware\SelectDateTimeMiddleware


## Credits
Boostrap3Mod built on top of awesome form builder by [AdamWathan\Form](https://github.com/adamwathan/form).
