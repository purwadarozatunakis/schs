<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableRelawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('t_pemadam', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('alamat');
            $table->string('usia');
            $table->string('kecamatan');
            $table->string('regu');
            $table->string('pekerjaan');
            $table->string('keterangan');
            $table->string('latitude');
            $table->string('longtitude');
            $table->string('status');
            $table->boolean('complete');
            $table->boolean('masuk_web');
            $table->boolean('masuk_aplikasi');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //\
        Schema::dropColumn('t_pemadam');
    }
}
