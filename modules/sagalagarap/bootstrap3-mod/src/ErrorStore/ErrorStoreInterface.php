<?php namespace Sagalagarap\Boostrap3Mod\ErrorStore;

interface ErrorStoreInterface
{
    public function hasError($key);
    public function getError($key);
}
