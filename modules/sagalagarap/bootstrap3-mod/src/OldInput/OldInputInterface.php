<?php namespace Sagalagarap\Boostrap3Mod\OldInput;

interface OldInputInterface
{
    public function hasOldInput();
    public function getOldInput($key);
}
