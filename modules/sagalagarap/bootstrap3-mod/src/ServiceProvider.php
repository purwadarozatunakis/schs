<?php

namespace Sagalagarap\Boostrap3Mod;

use Sagalagarap\Boostrap3Mod\Boostrap3Mod;
use Sagalagarap\Boostrap3Mod\ErrorStore\IlluminateErrorStore;
use Sagalagarap\Boostrap3Mod\OldInput\IlluminateOldInputProvider;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class PackageServiceProvider
 *
 * @package Sagalagarap\Boostrap3Mod
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('boostrap3-mod', function ($app) {

            $builder = new Boostrap3Mod();
            $builder->setToken($app['session.store']->token());
            $builder->setErrorStore(new IlluminateErrorStore($app['session.store']));
            $builder->setOldInputProvider(new IlluminateOldInputProvider($app['session.store']));

            return $builder;
        });
    }

    /**
     * Application is booting
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(realpath(__DIR__.'/../resources/views/'), 'boostrap3-mod');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['boostrap3-mod'];
    }
}

