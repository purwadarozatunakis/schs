<?php namespace Sagalagarap\Boostrap3Mod\Elements;

class Hidden extends Input
{
    protected $attributes = array(
        'type' => 'hidden',
        'class' => 'form-control',
    );
}
