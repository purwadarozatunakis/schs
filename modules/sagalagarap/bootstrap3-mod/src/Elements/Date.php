<?php namespace Sagalagarap\Boostrap3Mod\Elements;

class Date extends Text
{
    protected $attributes = array(
        'type' => 'date',
        'class' => 'form-control',
    );

    public function value($value)
    {
        if ($value instanceof \DateTime) {
            $value = $value->format('Y-m-d');
        }
        return parent::value($value);
    }
}
