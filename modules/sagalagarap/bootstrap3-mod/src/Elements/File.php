<?php namespace Sagalagarap\Boostrap3Mod\Elements;

class File extends Input
{
    protected $attributes = array(
        'type' => 'file',
        'class' => 'form-control',
    );
}
