<?php namespace Sagalagarap\Boostrap3Mod\Elements;

abstract class Input extends FormControl
{
    protected $attributes = [
        'class' => 'form-control'
    ];

    public function render()
    {
        if ($this->label) {
            $element = clone $this;
            $element->label = false;

            return (new Field($this->label, $element))->addClass($this->fieldWidth)->render();
        }

        $result = '<input';
        $result .= $this->renderAttributes();
        $result .= '>';
        $result .= $this->renderHint();

        return $result;
    }

    public function value($value)
    {
        $this->setValue($value);

        return $this;
    }

    protected function setValue($value)
    {
        $this->setAttribute('value', $value);

        return $this;
    }
}
