<?php namespace Sagalagarap\Boostrap3Mod\Elements;

class RadioButtonWrapper extends Wrapper
{
    protected $attributes = [
        'class' => 'ui radio checkbox'
    ];
}
