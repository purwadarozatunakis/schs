<?php namespace Sagalagarap\Boostrap3Mod\Elements;

class CheckboxWrapper extends Wrapper
{
    protected $attributes = [
        'class' => 'ui checkbox'
    ];
}
