<?php namespace Sagalagarap\Boostrap3Mod\Elements;

class Password extends Text
{
    protected $attributes = array(
        'type' => 'password',
        'class' => 'form-control',
    );
}
