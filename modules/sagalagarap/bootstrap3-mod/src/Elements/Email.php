<?php namespace Sagalagarap\Boostrap3Mod\Elements;

class Email extends Text
{
    protected $attributes = array(
        'type' => 'email',
        'class' => 'form-control',
    );
}
