<?php
namespace Sagalagarap\Boostrap3Mod;

class Facade extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'boostrap4-mod';
    }
}
