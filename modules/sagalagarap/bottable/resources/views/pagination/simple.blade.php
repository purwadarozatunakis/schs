<ul class="menu attached right bottom">
    <!-- Previous Page Link -->
    @if ($paginator->onFirstPage())
        <div class="item disabled">@lang('bottable::pagination.previous')</div>
    @else
        <a class="item" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('bottable::pagination.previous')</a>
    @endif

    <!-- Next Page Link -->
    @if ($paginator->hasMorePages())
        <a class="item" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('bottable::pagination.next')</a>
    @else
        <div class="item disabled">@lang('bottable::pagination.next')</div>
    @endif
</ul>
