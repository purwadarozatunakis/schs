
<ul class="pagination" style="margin:0px;">
    @if ($paginator->onFirstPage())
        <li class="page-item disabled"><a class="page-link" >@lang('bottable::pagination.previous')</a></li>
    @else
        
        <li class="page-item disabled"><a class="page-link"  href="{{ $paginator->previousPageUrl() }}">@lang('bottable::pagination.previous')</a></li>
    @endif
    
    
    <!-- Pagination Elements -->
    @foreach ($elements as $element)
    <!-- "Three Dots" Separator -->
        @if (is_string($element))
            
            <li class="page-item"><a class="page-link" href="#">{{$element}}</a></li>
        @endif

    <!-- Array Of Links -->
        @if (is_array($element))
            @foreach ($element as $page => $url)
                @if ($page == $paginator->currentPage())
                    <li class="page-item"><a class="page-link">{{$page}}</a></li>
                @else
                    <li class="page-item"><a class="page-link" href="{{$url}}">{{$page}}</a></li>
                @endif
            @endforeach
        @endif
    @endforeach


    @if ($paginator->hasMorePages())
    <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}">@lang('bottable::pagination.next')</a></li>

    @else

        <li class="page-item disabled"><a class="page-link" href="{{ $paginator->nextPageUrl() }}">@lang('bottable::pagination.next')</a></li>

    @endif
  </ul>