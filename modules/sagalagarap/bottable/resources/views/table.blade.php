<div id="{{ $id }}">

    <div class="ui menu top attached">
        @if($title)
            <div class="item borderless">
                <h4>{!! $title !!}</h4>
            </div>
        @endif
        <div class="item borderless">

        </div>
        <div class="right menu">
            @if($search)
                @include('bottable::toolbars.search', compact('search'))
            @endif
        </div>
            @if($toolbar)
                @include($toolbar)
            @endif
    </div>

    @if(!empty($toolbars))
        <div class="ui menu attached">
            @foreach($toolbars as $toolbar)
                <div class="item borderless">
                    {!! $toolbar !!}
                </div>
            @endforeach
            <div class="menu right">
            </div>
        </div>
    @endif

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="ui segment attached basic" style="padding: 0; overflow-y: auto">
                <table class="table table-bordered {{ $tableClass }}">
                    <thead>
                    <tr>
                        @foreach($headers as $header)
                            @if($header->isSortable())
                                {!! $header->getHtml() !!}
                            @else
                                <th {!! $header->renderAttributes() !!}>{!! $header->getHtml() !!}</th>
                            @endif
                        @endforeach
                    </tr>
                    </thead>
                    <tbody class="collection">
                    @forelse($collection as $key => $data)
                        @if($row)
                            
                            @include($row)
                        @else
                            <tr>
                                @foreach($fields as $field)
                                    <td {!! $builder->renderCellAttributes($field, $data) !!}>{!! $builder->renderCell($field, $data) !!}</td>
                                @endforeach
                            </tr>
                        @endif
                    @empty
                        @include('bottable::empty')
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel-footer">
            @if($showPagination)
                <div class="row">
                     @if(!$collection->isEmpty())
                        <div class="col-lg-9">
                        {!! $collection->appends(request()->input())->links($paginationView) !!}
                        </div>
                        <div class="col-lg-3 ">
                            <small class="pull-right">{{ $builder->summary() }}</small>
                        </div>
                    @endif    
                </div>
            @endif
        </div>
    </div>
</div>

@push(config('bottable.script_placeholder'))
@include('bottable::columns.checkall.script', compact('id'))
@endpush
