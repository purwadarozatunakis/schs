<?php

namespace Sagalagarap\Bottable\Tests;

use Illuminate\Database\Eloquent\Collection;
use Sagalagarap\Bottable\Builder;
use Sagalagarap\Bottable\Columns\Checkall;

class BuilderTest extends \Orchestra\Testbench\TestCase
{

    protected function getPackageProviders($app)
    {
        return [
            \Sagalagarap\Bottable\ServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Suitable' => \Sagalagarap\Bottable\Facade::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('mural.skin', 'semantic-ui');
    }

    /**
     * @test
     */
    public function it_can_be_created()
    {
        new Builder();
    }

    /**
     * @test
     */
    public function it_can_render_empty_collection()
    {
        $builder = new Builder();
        $collection = new Collection();

        $html = $builder->source($collection)->render();

    }


    /**
     * @test
     */
    public function it_can_render_checkall_column()
    {
        $builder = new Builder();
        $collection = new Collection();

        $builder->source($collection)
                ->columns([
                    new Checkall(),
                ])
                ->render();
    }
}
