<?php
namespace Sagalagarap\Bottable;

class Facade extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'sagalagarap.bottable';
    }
}
