<?php
namespace Sagalagarap\Bottable\Columns;

use Illuminate\Support\Facades\View;

class Numbers implements ColumnInterface
{
    private $attrs;

    protected $headerAttributes = ['width' => '50px'];

    public function __construct($attrs)
    {
        $this->attrs = $attrs;
    }

    private function getAttr($name)
    {
        if(isset($this->attrs[$name])){
            return $this->attrs[$name];
        }
        return null;
    }

    public function addAttrs($attrs)
    {
        $this->attrs = array_merge($this->attrs , $attrs);
    }

    public function header()
    {
        $header  = $this->getAttr('header');
        return View::make('bottable::columns.numbers.header', ['header' => $header ])->render();
    }

    public function headerAttributes()
    {
        return $this->headerAttributes;
    }

    public function cell($data, $index = 1)
    {
        $number = $index + $this->getAttr('from');
        return View::make('bottable::columns.numbers.cell' ,  compact('number'))->render();
    }
}
