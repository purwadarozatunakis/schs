<?php
namespace Sagalagarap\Bottable\Columns;

interface ColumnInterface
{
    public function header();

    public function headerAttributes();

    public function cell($cell , $index = 1);

    public function addAttrs($attrs);

    
}
