<?php
namespace Sagalagarap\Bottable\Columns;

use Illuminate\Support\Facades\View;
use Lavary\Menu\Menu;

class Dropdown implements ColumnInterface
{

    protected $text;

    protected $menus;

    protected $direction = 'right';

    private $attrs;

    /**
     * Dropdown constructor.
     */
    public function __construct($text, \Closure $closure)
    {
        $this->menus = (new Menu)->make($this->getId(), $closure);
        $this->setText($text);
    }

    public function header()
    {
        return '';
    }

    public function headerAttributes()
    {
        return null;
    }

    public function cell($cell, $index = 1)
    {
        $data['text'] = $this->text;
        $data['menus'] = $this->menus->roots();
        $data['direction'] = $this->direction;

        return View::make('bottable::columns.dropdown.cell', $data)->render();
    }

    public function addAttrs($attrs)
    {
        $this->attrs = array_merge($this->attrs , $attrs);
    }
    
    public function setDirection($direction)
    {
        $this->direction = $direction;

        return $this;
    }

    protected function getId()
    {
        return 'suitable-dropdown-'.str_random();
    }

    protected function setText($text)
    {
        if ($text) {
            $this->text = $text;
        }
    }
}
