<?php
namespace Sagalagarap\Bottable\Columns;

use Illuminate\Support\Facades\View;

class Checkall implements ColumnInterface
{
    private $filldata;

    protected $headerAttributes = ['width' => '50px'];

    private $attrs;

    public function __construct($filldata = array())
    {
        $this->filldata = $filldata;
    }

    private function isChecked($data){
        if (!$this->filldata) {
            return false;
        }
        return in_array($data->id, $this->filldata);
    }

    public function addAttrs($attrs)
    {
        $this->attrs = array_merge($this->attrs , $attrs);
    }

    public function header()
    {
        return View::make('bottable::columns.checkall.header')->render();
    }

    public function headerAttributes()
    {
        return $this->headerAttributes;
    }

    public function cell($data)
    {
        $checked = $this->isChecked($data);
        return View::make('bottable::columns.checkall.cell', compact('data', 'checked'))->render();
    }
}
