<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
</head>
<body>
    <div>
        <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <p>
                Schs.id</p>
                </a>
                
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">			
                
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="http://www.pingpong-labs.com" target="_blank">Visit Site</a></li>
                    <li class="dropdown ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Account
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

	<div class="container-fluid main-container">
		<div class="col-md-2 sidebar">

            @php
                $authenticatedMenu =  \Auth::user();
            @endphp 
            @if($authenticatedMenu->hasRole('ADMIN'))
            <div class="list-group">
                <a class="list-group-item active main-color-bg">
                    <span class="fa fa-dashboard" aria-hidden="true"></span> Administrator
                </a>
                
                <a class="list-group-item" href="{{ route('users.index') }}">User</a>
                <a class="list-group-item" href="{{ route('murid.index') }}">Murid</a>
                <a class="list-group-item" href="{{ route('guru.index') }}">Guru</a>
                <a class="list-group-item" href="{{ route('mapel.index') }}">Mapel</a>
                <a class="list-group-item" href="{{ route('jurusan.index') }}">Jurusan</a>
                <a class="list-group-item" href="{{ route('kelas.index') }}">Kelas</a>
                </div>
            @endif
            @if($authenticatedMenu->hasRole('GURU'))
            <div class="list-group">
                <a class="list-group-item active main-color-bg">
                    <span class="fa fa-dashboard" aria-hidden="true"></span> Guru
                </a>
                
                <a class="list-group-item" href="{{ route('absensi.index') }}">Absensi</a>
                <a class="list-group-item" href="{{ route('agenda.index') }}">Agenda</a>
                <a class="list-group-item" href="{{ route('tugas.index') }}">Tugas</a>
                <a class="list-group-item" href="{{ route('uas.index') }}">Uas</a>
                <a class="list-group-item" href="{{ route('tugas.index') }}">Uts</a>
                <a class="list-group-item" href="{{ route('rapot_siswa.index') }}">Report Siswa</a>
                </div>
            @endif
            @if($authenticatedMenu->hasRole('MURID'))
            <div class="list-group">
                <a class="list-group-item active main-color-bg">
                    <span class="fa fa-dashboard" aria-hidden="true"></span> Siswa
                </a>
                
                <a class="list-group-item" href="{{ route('murid.absensi.index') }}">Absensi</a>
                <a class="list-group-item" href="{{ route('murid.tugas.index') }}">Tugas</a>
                </div>
            @endif
		</div>
        <div class="col-md-10 content">
        
        @yield('content')
        </div>
		<footer class="pull-right footer">
			<p class="col-md-12" style="text-align:center">
				<hr class="divider">
				Copyright &COPY; {{date('Y')}} <a href="http://gamma.co.id">Gamma Solution</a>
			</p>
		</footer>
	</div>

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    @stack('scripts')
</body>
</html>
