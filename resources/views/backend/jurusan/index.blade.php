@extends('layouts.dashboard')

@section('content')

            
                    <div class="table-responsive">
                        {!! Bottable::source($data)
                        ->id('dataTable')
                        ->search(false)
                        ->toolbar('backend.jurusan.search')
                        ->columns([
                            ['header' => 'Kode Jurusan', 'field' => 'kode_jurusan'],
                            ['header' => 'Nama', 'field' => 'nama'],
                            ['header' => "Aksi", 'raw' => function($row){
                                return 
                                    B3m::open(route('jurusan.delete', [$row->kode_mapel])).
                                    "
                                        <input type='hidden' name='id' value='".$row->kode_mapel."'>
                                        <a class='btn btn-primary' href='". route('jurusan.ubah', ['id' => $row->kode_mapel."" ]) ."'>Edit</a>
                                        <button class='btn btn-primary'>Delete</button>
                                    ".
                                    B3m::close();
                            }]
                        ])
                        ->render() !!}
                    </div>
@endsection
