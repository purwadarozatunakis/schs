
{!! B3m::text('kode_jurusan', $data->kode_jurusan)->label('Kode Jurusan')->required(); !!}
{!! B3m::text('nama', $data->nama)->label('Nama')->required(); !!}
{!! B3m::select('kajur', $listGuru,  $data->kajur)->label('Kepala Jurusan')->prependOption('', 'Pilih Kepala Jurusan')->required();!!}
{!! B3m::textarea('keterangan', $data->keterangan)->label('Keterangan')->required();!!}

