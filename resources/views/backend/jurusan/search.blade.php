
{!! B3m::open(route('jurusan.index'))->get() !!}
<div class="panel panel-default">
  <div class="panel-heading"  style="min-height: 55px;">
    <div class="pull-left">
        <h4>Jurusan</h4>
    </div>
    <div class="pull-right">
        <a href="{!! route('jurusan.tambah') !!}" class="btn btn-primary">Tambah</a>
    </div>
  </div>
  <div class="panel-body">
        <div class="col-lg-6">  

        {!! B3m::text('search_kode_jurusan', request()->search_kode_jurusan)->label('Kode Jurusan')->id('search_kode_jurusan')->addClass('foo'); !!}
        
        {!! B3m::text('search_nama', request()->search_nama)->label('Nama Jurusan')->id('search_nama')->addClass('foo'); !!}
        
        </div>
  </div>
  <div class="panel-footer">
    {!! B3m::submit("Cari")->class('btn btn-primary') !!}
  </div>
</div>

{!! B3m::close() !!}