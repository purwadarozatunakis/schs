
{!! B3m::select('kode_kelas', $listKelas,  $data->kode_kelas)->label('Mata Pelajaran')->prependOption('', 'Pilih Kelas')->required();!!}
{!! B3m::select('kode_mapel', $listMapel,  $data->kode_mapel)->label('Mata Pelajaran')->prependOption('', 'Pilih Mata Pelajaran')->required();!!}
{!! B3m::select('jenis_tugas', ['Pekerjaan Rumah' => 'Pekerjaan Rumah' , 'Tugas Sekolah' => 'Tugas Sekolah'],  $data->jenis_tugas)->label('Jenis Tugas')->prependOption('', 'Pilih Jenis Tugas')->required();!!}
{!! B3m::date('tanggal_pengumpulan', $data->tanggal_pengumpulan ? $data->tanggal_pengumpulan : date('Y-m-d'))->label('Tanggal Pengumpulan')->required();!!}
{!! B3m::textarea('soal_tugas', $data->soal_tugas)->label('Soal Tugas')->required();!!}
