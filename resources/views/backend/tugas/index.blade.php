@extends('layouts.dashboard')

@section('content')

            
                    <div class="table-responsive">
                        {!! Bottable::source($data)
                        ->id('dataTable')
                        ->search(false)
                        ->toolbar('backend.tugas.search')
                        ->columns([
                            ['header' => 'Kode tugas', 'field' => 'kode_tugas'],
                            ['header' => 'Nama Kelas', 'raw' => function($row){
                                return $row->kelas->nama;
                            }],
                            ['header' => 'Nama Mapel', 'raw' => function($row){
                                return $row->mapel->nama;
                            }],
                            ['header' => 'Tanggal Pengumpulan', 'field' => 'tanggal_pengumpulan'],
                            ['header' => "Aksi", 'raw' => function($row){
                                return 
                                    B3m::open(route('tugas.delete', [$row->kode_tugas])).
                                    "
                                        <input type='hidden' name='id' value='".$row->kode_tugas."'>
                                        <a class='btn btn-primary' href='". route('tugas.ubah', ['id' => $row->kode_tugas."" ]) ."'>Edit</a>
                                        <a class='btn btn-primary' href='". route('tugas.beriNilai', ['id' => $row->kode_tugas."" ]) ."'>Beri Nilai</a>
                                        <button class='btn btn-primary'>Delete</button>
                                    ".
                                    B3m::close();
                            }]
                        ])
                        ->render() !!}
                    </div>
@endsection
