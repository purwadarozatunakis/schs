@extends('layouts.dashboard')

@section('content')

    {!! B3m::open(route('tugas.beriNilaiSave',['id' => $data->kode_tugas])) !!}
    <div class="panel panel-default">
        <div class="panel-heading"  style="min-height: 55px;">
            <div class="pull-left">
                <h4>Beli Nilai Tugas</h4>
            </div>
            <div class="pull-right">
                {!! B3m::submit("Simpan")->class('btn btn-primary') !!}
            </div>
        </div>
        <div class="panel-body">
                {!! B3m::text('kelas', $data->kelas->nama)->label('Kelas')->disable() !!}
                {!! B3m::text('mapel', $data->mapel->nama)->label('Mapel')->disable() !!}
                {!! B3m::text('tanggal_pengumpulan', $data->tanggal_pengumpulan)->label('Tanggal Pengumpulan')->disable() !!}

                <table class="table table-bordered">
                    <tr>
                        <th>
                            Kode Siswa
                        </th>
                        <th>
                            Nama Siswa
                        </th>
                        <th>
                            Nilai Tugas
                        </th>
                    </tr>
                    @foreach($listMurid as $murid)
                    <tr>
                        <td>
                            {{ $murid->kode_murid }}
                        </td>
                        <td>
                        
                            {{ $murid->nama }}
                        </td>
                        <td>
                            {!! B3m::text('nilai['.$murid->kode_murid.']', $murid->nilaiTugas($data->kode_tugas)->nilai )!!}
                
                        </td>
                        
                    </tr>
                    @endforeach
                </table>
        </div>
        <div class="panel-footer"  style="min-height: 55px;">
                
            <div class="pull-right">
                {!! B3m::submit("Simpan")->class('btn btn-primary') !!}
            </div>

        </div>
    </div>

    {!! B3m::close() !!}
@endsection
