<div class="table-responsive">

<input type="hidden" name="kelas"  value="{{request()->kode_kelas}}" >
<input type="hidden" name="guru"  value="{{request()->kode_guru}}" >
<input type="hidden" name="mapel"  value="{{request()->kode_mapel}}" >

<table class="table table-bordered ">
    <thead>
    <tr>
        <th>Kode Murid</th>
        <th>Nama</th>
        <th>Keteragan</th>
        <th>Keteragan Tambahan</th>
    </tr>
    </thead>
    <tbody class="collection">
        @foreach ($listMurid as $murid)
        <tr>
            <td>{{ $murid->kode_murid }}</td>
            <td>{{ $murid->nama }}</td>
            <td>
            
                <input type="hidden" name="murid[]" value="{{ $murid->kode_murid }}">
                <input type="hidden" name="keterangan[{{ $murid->kode_murid }}]" id="keterangan_{{ $murid->kode_murid }}" value="{!! $murid->absenMuridHariIni($absensi) !!}">
                <a class="btn {!! $murid->absenMuridHariIni($absensi) == 'masuk' ? ' btn-primary  ' : ' btn-default ' !!} masuk keterangan_{{ $murid->kode_murid }}" onclick="setKeterangan('masuk', '{{ $murid->kode_murid }}')"> Masuk</a>
                <a class="btn {!! $murid->absenMuridHariIni($absensi) == 'tidak_masuk' ? ' btn-primary  ' : ' btn-default ' !!} tidak_masuk keterangan_{{ $murid->kode_murid }}" onclick="setKeterangan('tidak_masuk', '{{ $murid->kode_murid }}')"> Tidak Masuk</a>
                <a class="btn {!! $murid->absenMuridHariIni($absensi) == 'izin' ? ' btn-primary ' : ' btn-default ' !!} izin keterangan_{{ $murid->kode_murid }}" onclick="setKeterangan('izin', '{{ $murid->kode_murid }}')">Izin</a>
                <a class="btn {!! $murid->absenMuridHariIni($absensi) == 'sakit' ? ' btn-primary ' : ' btn-default ' !!} sakit keterangan_{{ $murid->kode_murid }}" onclick="setKeterangan('sakit', '{{ $murid->kode_murid }}')">Sakit</a>
            </td>
            <td>
                <input type="text" class="form-control" name="keterangan_tambahan[{{ $murid->kode_murid }}]" value="{!! isset($absensi[$murid->kode_murid]) ? $absensi[$murid->kode_murid]['data']->keterangan_tambahan : ''!!}">
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>

@push('scripts')
    <script>
        function setKeterangan(keterangan, kode_murid) {
            $('#keterangan_' + kode_murid + '').val(keterangan);
            $('.keterangan_' + kode_murid + '').removeClass('btn-primary');
            $('.keterangan_' + kode_murid + '').addClass('btn-default');
            $('.keterangan_' + kode_murid + '.' + keterangan).addClass('btn-primary')
        }
    </script>
@endpush