@extends('layouts.dashboard')

@section('content')

            
                    <div class="table-responsive">
                        {!! Bottable::source($data)
                        ->id('dataTable')
                        ->search(false)
                        ->toolbar('backend.guru.search')
                        ->columns([
                            ['header' => 'NIP', 'field' => 'nip'],
                            ['header' => 'Nama', 'field' => 'nama'],
                            ['header' => 'Email', 'field' => 'email'],
                            ['header' => "Aksi", 'raw' => function($row){
                                return 
                                    B3m::open(route('guru.delete', [$row->nip])).
                                    "
                                        <input type='hidden' name='id' value='".$row->nip."'>
                                        <a class='btn btn-primary' href='". route('guru.ubah', ['id' => $row->nip."" ]) ."'>Edit</a>
                                        <button class='btn btn-primary'>Delete</button>
                                    ".
                                    B3m::close();
                            }]
                        ])
                        ->render() !!}
                    </div>
@endsection
