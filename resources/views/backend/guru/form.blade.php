
{!! B3m::text('nip', $data->nip)->label('NIP'); !!}
{!! B3m::text('nama', $data->nama)->label('Nama'); !!}
{!! B3m::email('email', $data->email)->label('Email'); !!}
{!! B3m::text('no_tlp', $data->no_tlp)->label('No Telepon'); !!}
{!! B3m::password('password')->label('Password')->placeholder('Password'); !!}
