
{!! B3m::open(route('guru.index'))->get() !!}
<div class="panel panel-default">
  <div class="panel-heading"  style="min-height: 55px;">
    <div class="pull-left">
        <h4>Guru</h4>
    </div>
    <div class="pull-right">
        <a href="{!! route('guru.tambah') !!}" class="btn btn-primary">Tambah</a>
    </div>
  </div>
  <div class="panel-body">
        <div class="col-lg-6">

        {!! B3m::text('search_nip', request()->search_nip)->label('NIP')->id('search_nip')->addClass('foo'); !!}
        
        {!! B3m::text('search_nama', request()->search_nama)->label('Nama')->id('search_nama')->addClass('foo'); !!}
        
        </div>
  </div>
  <div class="panel-footer">
    {!! B3m::submit("Cari")->class('btn btn-primary') !!}
  </div>
</div>

{!! B3m::close() !!}