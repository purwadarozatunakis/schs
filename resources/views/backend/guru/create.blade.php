@extends('layouts.dashboard')

@section('content')

    {!! B3m::open(route('guru.tambahSave')) !!}
    <div class="panel panel-default">
        <div class="panel-heading"  style="min-height: 55px;">
            <div class="pull-left">
                <h4>Guru</h4>
            </div>
            <div class="pull-right">
                {!! B3m::submit("Simpan")->class('btn btn-primary') !!}
            </div>
        </div>
        <div class="panel-body">
                
                @include('backend.guru.form')

        </div>
        <div class="panel-footer"  style="min-height: 55px;">
                
            <div class="pull-right">
                {!! B3m::submit("Simpan")->class('btn btn-primary') !!}
            </div>

        </div>
    </div>

    {!! B3m::close() !!}
@endsection
