@extends('layouts.dashboard')

@section('content')

            
                    <div class="table-responsive">
                        {!! Bottable::source($data)
                        ->id('dataTable')
                        ->search(false)
                        ->toolbar('backend.rapot_siswa.search')
                        ->columns([
                            ['header' => 'Kode Kelas', 'raw' => function($row){
                                return $row->kelas ? $row->kelas->nama : '';
                            }],
                            ['header' => 'Mata Pelajaran', 'raw' => function($row){
                                return $row->mapel ? $row->mapel->nama : '';
                            }],
                            ['header' => 'Nama Guru Pengajar', 'raw' => function($row){
                                return $row->guru ? $row->guru->nama : '';
                            }],
                            ['header' => "Aksi", 'raw' => function($row){
                                return 
                                    "
                                        <a class='btn btn-primary' href='". route('rapot_siswa.tambah', ['kode_kelas' => $row->kode_kelas, 'kode_mapel' => $row->kode_mapel]) ."'>Lihat Kelas</a>
                                    ";
                            }]
                        ])
                        ->render() !!}
                    </div>
@endsection
