@extends('layouts.dashboard')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"  style="min-height: 55px;">
            <div class="pull-left">
                <h4>Report {{ request()->tanggal ? : date('Y-m-d')}}
                    Kelas  {{ $kelas->nama }}
                    Mata Pelajaran  {{ $mapel->nama }}
                </h4>
            </div>
        </div>
        <div class="panel-body">
            
                @include('backend.rapot_siswa.form')
            
        </div>
    </div>

    {!! B3m::close() !!}
@endsection
