<div class="table-responsive">

<input type="hidden" name="kelas"  value="{{request()->kode_kelas}}" >
<input type="hidden" name="guru"  value="{{request()->kode_guru}}" >
<input type="hidden" name="mapel"  value="{{request()->kode_mapel}}" >

<table class="table table-bordered ">
    <thead>
    <tr>
        <th>Kode Murid</th>
        <th>Nama</th>
        <th>Nilai UTS</th>
        <th>Nilai UAS</th>
        <th>Nilai Tugas</th>
        <th>Nilai Rata Rata</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody class="collection">
        @foreach ($listMurid as $murid)
        <tr>
            <td>{{ $murid->kode_murid }}</td>
            <td>{{ $murid->nama }}</td>
            <td>{{ $murid->nama }}</td>
            <td>{{ $murid->nama }}</td>
            <td>{{ $murid->nama }}</td>
            <td>{{ $murid->nama }}</td>
            
            <td>
                <a class='btn btn-primary' href='{{ route('rapot_siswa.murid.lihat_nilai', ['kode_kelas' => request()->kode_kelas, 'kode_mapel' => request()->kode_mapel, 'kode_murid' => $murid->kode_murid]) }}'>Lihat Nilai</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>