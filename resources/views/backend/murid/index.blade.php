@extends('layouts.dashboard')

@section('content')

            
                    <div class="table-responsive">
                        {!! Bottable::source($data)
                        ->id('dataTable')
                        ->search(false)
                        ->toolbar('backend.murid.search')
                        ->columns([
                            ['header' => 'Kode Murid', 'field' => 'kode_murid'],
                            ['header' => 'Nama', 'field' => 'nama'],
                            ['header' => 'Email', 'field' => 'email'],
                            ['header' => "Aksi", 'raw' => function($row){
                                return 
                                    B3m::open(route('murid.delete', [$row->kode_murid])).
                                    "
                                        <input type='hidden' name='id' value='".$row->kode_murid."'>
                                        <a class='btn btn-primary' href='". route('murid.ubah', ['id' => $row->kode_murid."" ]) ."'>Edit</a>
                                        <button class='btn btn-primary'>Delete</button>
                                    ".
                                    B3m::close();
                            }]
                        ])
                        ->render() !!}
                    </div>
@endsection
