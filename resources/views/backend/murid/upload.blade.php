@extends('layouts.dashboard')

@section('content')

    {!! B3m::open(route('murid.tambahUploadSave'))->multipart()  !!}
    <div class="panel panel-default">
        <div class="panel-heading"  style="min-height: 55px;">
            <div class="pull-left">
                <h4>Upload Data Murid</h4>
            </div>
            <div class="pull-right">
                {!! B3m::submit("Simpan")->class('btn btn-primary')!!}
            </div>
        </div>
        <div class="panel-body">

            {!! B3m::select('kode_kelas', $listKelas,  $data->kode_kelas)->label('Kelas')->prependOption('', 'Pilih Kelas')->required();!!}

            {!! B3m::file('file')->label('Upload File Batch')->required(); !!}

        </div>
        <div class="panel-footer"  style="min-height: 55px;">
                
            <div class="pull-right">
                {!! B3m::submit("Simpan")->class('btn btn-primary') !!}
            </div>

        </div>
    </div>

    {!! B3m::close() !!}
@endsection
