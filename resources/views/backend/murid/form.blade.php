
{!! B3m::text('kode_murid', $data->kode_murid)->label('Kode Murid')->required(); !!}
{!! B3m::text('nama', $data->nama)->label('Nama')->required(); !!}
{!! B3m::email('email', $data->email)->label('Email')->required(); !!}
{!! B3m::text('no_tlp', $data->no_tlp)->label('No Telepon')->required(); !!}
{!! B3m::password('password')->label('Password')->placeholder('Password'); !!}
{!! B3m::select('kode_kelas', $listKelas,  $data->kode_kelas)->label('Kelas')->prependOption('', 'Pilih Kelas')->required();!!}
