
{!! B3m::open(route('murid.index'))->get() !!}
<div class="panel panel-default">
  <div class="panel-heading"  style="min-height: 55px;">
    <div class="pull-left">
        <h4>Murid</h4>
    </div>
    <div class="pull-right">
        <a href="{!! route('murid.tambah') !!}" class="btn btn-primary">Tambah</a>

        <a href="{!! route('murid.tambahUpload') !!}" class="btn btn-primary">Upload List Murid</a>
    </div>
  </div>
  <div class="panel-body">
        <div class="col-lg-6">

        {!! B3m::text('search_kode_murid', request()->search_kode_murid)->label('Kode  Murid')->id('search_kode_murid')->addClass('foo'); !!}
        
        {!! B3m::text('search_nama', request()->search_nama)->label('Nama')->id('search_nama')->addClass('foo'); !!}
        
        </div>
  </div>
  <div class="panel-footer">
    {!! B3m::submit("Cari")->class('btn btn-primary') !!}
  </div>
</div>

{!! B3m::close() !!}