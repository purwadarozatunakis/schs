@extends('layouts.dashboard')

@section('content')

            
                    <div class="table-responsive">
                        {!! Bottable::source($data)
                        ->id('dataTable')
                        ->search(false)
                        ->toolbar('backend.mapel.search')
                        ->columns([
                            ['header' => 'Kode Mapel', 'field' => 'kode_mapel'],
                            ['header' => 'Nama', 'field' => 'nama'],
                            ['header' => 'Kode Guru', 'field' => 'kode_guru'],
                            ['header' => 'Type Mapel', 'field' => 'type_mapel'],
                            ['header' => "Aksi", 'raw' => function($row){
                                return 
                                    B3m::open(route('mapel.delete', [$row->kode_mapel])).
                                    "
                                        <input type='hidden' name='id' value='".$row->kode_mapel."'>
                                        <a class='btn btn-primary' href='". route('mapel.ubah', ['id' => $row->kode_mapel."" ]) ."'>Edit</a>
                                        <button class='btn btn-primary'>Delete</button>
                                    ".
                                    B3m::close();
                            }]
                        ])
                        ->render() !!}
                    </div>
@endsection
