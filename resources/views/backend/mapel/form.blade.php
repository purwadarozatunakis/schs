
{!! B3m::text('kode_mapel', $data->kode_mapel)->label('Kode Mapel')->required(); !!}
{!! B3m::text('nama', $data->nama)->label('Nama')->required(); !!}
{!! B3m::select('kode_guru', $listGuru,  $data->kode_guru)->label('Guru')->prependOption('', 'Pilih Guru')->required();!!}
{!! B3m::select('type_mapel', $jenisMapel,  $data->type_mapel)->label('Type Mapel')->prependOption('', 'Pilih Type Mapel')->required();!!}

