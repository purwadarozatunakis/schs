
{!! B3m::open(route('mapel.index'))->get() !!}
<div class="panel panel-default">
  <div class="panel-heading"  style="min-height: 55px;">
    <div class="pull-left">
        <h4>Mapel</h4>
    </div>
    <div class="pull-right">
        <a href="{!! route('mapel.tambah') !!}" class="btn btn-primary">Tambah</a>
    </div>
  </div>
  <div class="panel-body">
        <div class="col-lg-6">

        {!! B3m::text('search_kode_mapel', request()->search_kode_mapel)->label('Kode Mapel')->id('search_kode_mapel')->addClass('foo'); !!}
        
        {!! B3m::text('search_nama', request()->search_nama)->label('Nama Mapel')->id('search_nama')->addClass('foo'); !!}
        
        </div>
  </div>
  <div class="panel-footer">
    {!! B3m::submit("Cari")->class('btn btn-primary') !!}
  </div>
</div>

{!! B3m::close() !!}