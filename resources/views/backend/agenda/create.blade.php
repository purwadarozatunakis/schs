@extends('layouts.dashboard')

@section('content')

    {!! B3m::open(route('agenda.tambahSave', ['kode_kelas' => request()->kode_kelas, 'kode_mapel' => request()->kode_mapel, 'tanggal' => request()->tanggal])) !!}
    <div class="panel panel-default">
        <div class="panel-heading"  style="min-height: 55px;">
            <div class="pull-left">
                <h4>Agenda {{ request()->tanggal ? : date('d/m/Y')}}
                    Kelas  {{ $kelas->nama }}
                    Mata Pelajaran  {{ $mapel->nama }}
                </h4>
            </div>
            <div class="pull-right">
                {!! B3m::submit("Simpan")->class('btn btn-primary') !!}
            </div>
        </div>
        <div class="panel-body">
                
            <div class="col-lg-9">
                @include('backend.agenda.form')
            </div>
            <div class="col-lg-3">
                <div class="list-group">
                    <a class="list-group-item active main-color-bg">
                        <span class="fa fa-dashboard" aria-hidden="true"></span> Agenda Lainnya
                    </a>
                    <a class="list-group-item" href="
                    {{ route('agenda.tambah', ['kode_kelas' => request()->kode_kelas , 'kode_mapel' => request()->kode_mapel])  }}
                    ">Hari Ini</a>
                    @foreach ($listAgenda as $agendaLain)
                        <a class="list-group-item" href="
                        {{ route('agenda.tambah', ['kode_kelas' => request()->kode_kelas , 'kode_mapel' => request()->kode_mapel, 'tanggal' => $agendaLain->formatedCreatedDate('Y-m-d')])  }}
                        ">{{$agendaLain->formatedCreatedDate('Y-m-d')}}</a>
                    @endforeach
                </div>
            </div>

        </div>
        <div class="panel-footer"  style="min-height: 55px;">
                
            <div class="pull-right">
                {!! B3m::submit("Simpan")->class('btn btn-primary') !!}
            </div>

        </div>
    </div>

    {!! B3m::close() !!}
@endsection
