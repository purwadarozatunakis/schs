@extends('layouts.dashboard')

@section('content')

    {!! B3m::open(route('uas.beriNilaiSave')) !!}

    <input type="hidden" name="kelas"  value="{{request()->kode_kelas}}" >
    <input type="hidden" name="guru"  value="{{request()->kode_guru}}" >
    <input type="hidden" name="mapel"  value="{{request()->kode_mapel}}" >
    <div class="panel panel-default">
        <div class="panel-heading"  style="min-height: 55px;">
            <div class="pull-left">
                <h4>Beli Nilai Uas</h4>
            </div>
            <div class="pull-right">
                {!! B3m::submit("Simpan")->class('btn btn-primary') !!}
            </div>
        </div>
        <div class="panel-body">

                <table class="table table-bordered">
                    <tr>
                        <th>
                            Kode Siswa
                        </th>
                        <th>
                            Nama Siswa
                        </th>
                        <th>
                            Nilai Uas
                        </th>
                    </tr>
                    @foreach($listMurid as $murid)
                    <tr>
                        <td>
                            {{ $murid->kode_murid }}
                        </td>
                        <td>
                        
                            {{ $murid->nama }}
                        </td>
                        <td>
                            {!! B3m::text('nilai['.$murid->kode_murid.']')!!}
                
                        </td>
                        
                    </tr>
                    @endforeach
                </table>
        </div>
        <div class="panel-footer"  style="min-height: 55px;">
                
            <div class="pull-right">
                {!! B3m::submit("Simpan")->class('btn btn-primary') !!}
            </div>

        </div>
    </div>

    {!! B3m::close() !!}
@endsection
