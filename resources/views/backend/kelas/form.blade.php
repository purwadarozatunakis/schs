{!! B3m::text('kode_kelas', $data->kode_kelas)->label('Kode Kelas')->required(); !!}
{!! B3m::text('nama', $data->nama)->label('Nama')->required(); !!}
{!! B3m::text('tingkat', $data->tingkat)->label('Tingkat')->required(); !!}
{!! B3m::select('wali_kelas', $listGuru,  $data->wali_kelas)->label('Wali Kelas')->prependOption('', 'Pilih Wali Kelas')->required();!!}
{!! B3m::select('kode_jurusan', $listJurusan,  $data->kode_jurusan)->label('Jurusan')->prependOption('', 'Pilih Jurusan')->required();!!}

<label for="">Mata Pelajaran</label>
<table class="table table-bordered" id="mapelDanGuru">
    <tr>
        <th>
            Kode Mapel - Nama Mapel
        </th>
        <th>
            Guru
        </th>
        <th>
            <a  class="btn btn-primary" v-on:click="addRow">Tambah</a>
        </th>
    </tr>
    <tr v-for="(mapel , index) in listMapel">
        <td>
            {!! B3m::select('listMapel[]', $listMapel)->prependOption('-', 'Pilih Mata Pelajaran')->attribute('v-model', 'listMapel[index].kode_mapel')->required();!!}
        </td>
        <td>
            {!! B3m::select('listGuru[]', $listGuru)->prependOption('-', 'Pilih Guru')->attribute('v-model', 'listMapel[index].kode_guru')->required();!!}
        </td>
        <td>
            <a class="btn btn-danger" v-on:click="removeRow(index)">Hapus</a>
        </td>
    </tr>



</table>

@push('scripts')
<script>
    new Vue({
        el: '#mapelDanGuru',
        data: {
            listMapel : {!! $data->mapel !!}
        },
        methods: {
            addRow: function(){
                this.listMapel.push({'kode_guru' : '-', 'kode_mapel' : '-'})
            },
            removeRow: function(index){
                this.listMapel.splice(index,1)
            },
        }
    });
</script>
@endpush    