@extends('layouts.dashboard')

@section('content')

            
                    <div class="table-responsive">
                        {!! Bottable::source($data)
                        ->id('dataTable')
                        ->search(false)
                        ->toolbar('backend.kelas.search')
                        ->columns([
                            ['header' => 'Kode Kelas', 'field' => 'kode_kelas'],
                            ['header' => 'Nama', 'field' => 'nama'],
                            ['header' => "Aksi", 'raw' => function($row){
                                return 
                                    B3m::open(route('kelas.delete', [$row->kode_kelas])).
                                    "
                                        <input type='hidden' name='id' value='".$row->kode_kelas."'>
                                        <a class='btn btn-primary' href='". route('kelas.ubah', ['id' => $row->kode_kelas."" ]) ."'>Edit</a>
                                        <button class='btn btn-primary'>Delete</button>
                                    ".
                                    B3m::close();
                            }]
                        ])
                        ->render() !!}
                    </div>
@endsection
