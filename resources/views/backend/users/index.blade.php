@extends('layouts.dashboard')

@section('content')

            
                    <div class="table-responsive">
                        {!! Bottable::source($data)
                        ->id('dataTable')
                        ->search(false)
                        ->toolbar('backend.users.search')
                        ->columns([
                            ['header' => 'Nama', 'field' => 'name'],
                            ['header' => 'Email', 'field' => 'email'],
                            ['header' => "Aksi", 'raw' => function($row){
                                return 
                                    B3m::open(route('users.delete', [$row->id])).
                                    "
                                        <input type='hidden' name='id' value='".$row->id."'>
                                        <a class='btn btn-primary' href='". route('users.ubah', ['id' => $row->id."" ]) ."'>Edit</a>
                                        <button class='btn btn-primary'>Delete</button>
                                    ".
                                    B3m::close();
                            }]
                        ])
                        ->render() !!}
                    </div>
@endsection
