@extends('layouts.dashboard')

@section('content')

            
                    <div class="table-responsive">
                        {!! Bottable::source($data)
                        ->id('dataTable')
                        ->search(false)
                        ->toolbar('murid.absensi.search')
                        ->columns([
                            ['header' => 'Nama Mapel', 'raw' => function($row){
                                return $row->mapel->nama;
                            }],
                            ['header' => 'Kelas', 'raw' => function($row){
                                return $row->kelas->nama;
                            }],
                            ['header' => 'Tanggal', 'field' => 'created_at'],
                            ['header' => 'Keterangan', 'field' => 'keterangan'],
                            ['header' => 'Keterangan Tambahan', 'field' => 'keterangan_tambahan'],
                           
                        ])
                        ->render() !!}
                    </div>
@endsection
