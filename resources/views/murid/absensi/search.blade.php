
{!! B3m::open(route('murid.absensi.index'))->get() !!}
<div class="panel panel-default">
  <div class="panel-heading"  style="min-height: 55px;">
    <div class="pull-left">
        <h4>Absensi</h4>
    </div>
  </div>
  <div class="panel-body">
        <div class="col-lg-6">  

        {!! B3m::text('search_kode_smester', request()->search_kode_smester)->label('Kode Semester')->id('search_kode_smester')->addClass('foo'); !!}
        
        {!! B3m::text('search_tahun_ajaran', request()->search_tahun_ajaran)->label('Tahun Ajaran')->id('search_tahun_ajaran')->addClass('foo'); !!}
        
        </div>
  </div>
  <div class="panel-footer">
    {!! B3m::submit("Cari")->class('btn btn-primary') !!}
  </div>
</div>

{!! B3m::close() !!}