<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect('welcome.index');
// });

Route::get('/', 'WelcomeController@index')->name('welcome.index');



Auth::routes();



Route::get('/home', 'HomeController@index')->middleware(['auth'])->name('home');

Route::prefix('backend')->middleware(['auth','rac:ADMIN'])->group(function () {
    
    Route::prefix('users')->group(function () {
        Route::get('/', 'Backend\UserController@index')->name('users.index');
        Route::get('/tambah', 'Backend\UserController@create')->name('users.tambah');
        Route::post('/tambahSave', 'Backend\UserController@createSave')->name('users.tambahSave');
        Route::get('/{id}', 'Backend\UserController@detail')->name('users.detail');
        Route::get('/{id}/ubah', 'Backend\UserController@edit')->name('users.ubah');
        Route::post('/{id}/ubahSave', 'Backend\UserController@editSave')->name('users.ubahSave');
        Route::post('/{id}/delete', 'Backend\UserController@delete')->name('users.delete');
    });
    
    Route::prefix('murid')->group(function () {
        Route::get('/', 'Backend\MuridController@index')->name('murid.index');
        Route::get('/tambah', 'Backend\MuridController@create')->name('murid.tambah');
        Route::get('/tambahUpload', 'Backend\MuridController@createUpload')->name('murid.tambahUpload');
        Route::post('/tambahUploadSave', 'Backend\MuridController@createUploadSave')->name('murid.tambahUploadSave');
        Route::post('/tambahSave', 'Backend\MuridController@createSave')->name('murid.tambahSave');
        Route::get('/{id}', 'Backend\MuridController@detail')->name('murid.detail');
        Route::get('/{id}/ubah', 'Backend\MuridController@edit')->name('murid.ubah');
        Route::post('/{id}/ubahSave', 'Backend\MuridController@editSave')->name('murid.ubahSave');
        Route::post('/{id}/delete', 'Backend\MuridController@delete')->name('murid.delete');
    });

    Route::prefix('guru')->group(function () {
        Route::get('/', 'Backend\GuruController@index')->name('guru.index');
        Route::get('/tambah', 'Backend\GuruController@create')->name('guru.tambah');
        Route::post('/tambahSave', 'Backend\GuruController@createSave')->name('guru.tambahSave');
        Route::get('/{id}', 'Backend\GuruController@detail')->name('guru.detail');
        Route::get('/{id}/ubah', 'Backend\GuruController@edit')->name('guru.ubah');
        Route::post('/{id}/ubahSave', 'Backend\GuruController@editSave')->name('guru.ubahSave');
        Route::post('/{id}/delete', 'Backend\GuruController@delete')->name('guru.delete');
    });
    Route::prefix('mapel')->group(function () {
        Route::get('/', 'Backend\MapelController@index')->name('mapel.index');
        Route::get('/tambah', 'Backend\MapelController@create')->name('mapel.tambah');
        Route::post('/tambahSave', 'Backend\MapelController@createSave')->name('mapel.tambahSave');
        Route::get('/{id}', 'Backend\MapelController@detail')->name('mapel.detail');
        Route::get('/{id}/ubah', 'Backend\MapelController@edit')->name('mapel.ubah');
        Route::post('/{id}/ubahSave', 'Backend\MapelController@editSave')->name('mapel.ubahSave');
        Route::post('/{id}/delete', 'Backend\MapelController@delete')->name('mapel.delete');
    });
    Route::prefix('jurusan')->group(function () {
        Route::get('/', 'Backend\JurusanController@index')->name('jurusan.index');
        Route::get('/tambah', 'Backend\JurusanController@create')->name('jurusan.tambah');
        Route::post('/tambahSave', 'Backend\JurusanController@createSave')->name('jurusan.tambahSave');
        Route::get('/{id}', 'Backend\JurusanController@detail')->name('jurusan.detail');
        Route::get('/{id}/ubah', 'Backend\JurusanController@edit')->name('jurusan.ubah');
        Route::post('/{id}/ubahSave', 'Backend\JurusanController@editSave')->name('jurusan.ubahSave');
        Route::post('/{id}/delete', 'Backend\JurusanController@delete')->name('jurusan.delete');
    });
    Route::prefix('kelas')->group(function () {
        Route::get('/', 'Backend\KelasController@index')->name('kelas.index');
        Route::get('/tambah', 'Backend\KelasController@create')->name('kelas.tambah');
        Route::post('/tambahSave', 'Backend\KelasController@createSave')->name('kelas.tambahSave');
        Route::get('/{id}', 'Backend\KelasController@detail')->name('kelas.detail');
        Route::get('/{id}/ubah', 'Backend\KelasController@edit')->name('kelas.ubah');
        Route::post('/{id}/ubahSave', 'Backend\KelasController@editSave')->name('kelas.ubahSave');
        Route::post('/{id}/delete', 'Backend\KelasController@delete')->name('kelas.delete');
    });

});


Route::prefix('guru')->middleware(['auth','rac:GURU'])->group(function () {
    
    Route::prefix('absensi')->group(function () {
        Route::get('/', 'Backend\AbsensiController@index')->name('absensi.index');
        Route::get('{kode_kelas}/{kode_mapel}/isiAbsen/{tanggal?}', 'Backend\AbsensiController@create')->name('absensi.tambah');
        Route::post('{kode_kelas}/{kode_mapel}/simpanAbsen/{tanggal?}', 'Backend\AbsensiController@createSave')->name('absensi.tambahSave');
        Route::get('/{id}', 'Backend\AbsensiController@detail')->name('absensi.detail');
    });
    Route::prefix('rapor')->group(function () {
        Route::get('/', 'Backend\RaporSiswaController@index')->name('rapot_siswa.index');
        Route::get('{kode_kelas}/{kode_mapel}/detail/{tanggal?}', 'Backend\RaporSiswaController@detailSiswa')->name('rapot_siswa.tambah');
        Route::get('/{id}', 'Backend\RaporSiswaController@detail')->name('rapot_siswa.detail');

        Route::get('{kode_kelas}/{kode_mapel}/lihat_nilai/{kode_murid}', 'Backend\RaporSiswaController@beriNilaiSiswa')->name('rapot_siswa.murid.lihat_nilai');
        Route::post('{kode_kelas}/{kode_mapel}/beriNilai/{kode_murid}/simpan', 'Backend\RaporSiswaController@createSave')->name('rapot_siswa.murid.beriNilaiSave');
        
    });
    Route::prefix('agenda')->group(function () {
        Route::get('/', 'Backend\AgendaController@index')->name('agenda.index');
        Route::get('{kode_kelas}/{kode_mapel}/agendaKelas/{tanggal?}', 'Backend\AgendaController@create')->name('agenda.tambah');
        Route::post('{kode_kelas}/{kode_mapel}/simpanAgendaKelas/{tanggal?}', 'Backend\AgendaController@createSave')->name('agenda.tambahSave');
        Route::get('/{id}', 'Backend\AgendaController@detail')->name('agenda.detail');
    });

    Route::prefix('tugas')->group(function () {
        Route::get('/', 'Backend\TugasController@index')->name('tugas.index');
        Route::get('/tambah', 'Backend\TugasController@create')->name('tugas.tambah');
        Route::post('/tambahSave', 'Backend\TugasController@createSave')->name('tugas.tambahSave');
        Route::get('/{id}', 'Backend\TugasController@detail')->name('tugas.detail');
        Route::get('/{id}/ubah', 'Backend\TugasController@edit')->name('tugas.ubah');
        Route::post('/{id}/ubahSave', 'Backend\TugasController@editSave')->name('tugas.ubahSave');
        Route::get('/{id}/beriNilai', 'Backend\TugasController@beriNilai')->name('tugas.beriNilai');
        Route::post('/{id}/beriNilaiSave', 'Backend\TugasController@beriNilaiSave')->name('tugas.beriNilaiSave');
        Route::post('/{id}/delete', 'Backend\TugasController@delete')->name('tugas.delete');
    });

    Route::prefix('uas')->group(function () {
        Route::get('/', 'Backend\UasController@index')->name('uas.index');
        Route::get('/tambah', 'Backend\UasController@create')->name('uas.tambah');
        Route::post('/tambahSave', 'Backend\UasController@createSave')->name('uas.tambahSave');
        Route::get('/beriNilai', 'Backend\UasController@beriNilai')->name('uas.beriNilai');
        Route::post('/beriNilaiSave', 'Backend\UasController@beriNilaiSave')->name('uas.beriNilaiSave');
    });

});

Route::prefix('murid')->middleware(['auth','rac:MURID'])->group(function () {
    
    Route::prefix('absensi')->group(function () {
        Route::get('/', 'Murid\MuridAbsensiController@index')->name('murid.absensi.index');
    });
    Route::prefix('tugas')->group(function () {
        Route::get('/', 'Murid\MuridAbsensiController@index')->name('murid.tugas.index');
    });

});