<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    //
    public function index(Request $request)
    {
        # code...
        return view('landing-page.welcome');
    }
    
    public function register(Request $request)
    {
        # code...
        return view('landing-page.register');
    }
}
