<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Guru;
use App\User;

class GuruController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Guru::searchAndPaginate(['nip','nama'], $request, "search_", 20);
        return view('backend/guru/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Guru();
        return view('backend/guru/create', compact('data'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSave(Request $request)
    {
        $data = $request->all();
        $guru = Guru::create($data);
        $user = User::create([
            'name' => $guru->nama,
            'email' => $guru->email,
            'password' => bcrypt($data['password']),
            'type_user' => 'GURU',
        ]);
        return redirect(route('guru.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data =  Guru::find($id);
        return view('backend/guru/edit', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function editSave(Request $request, $id)
    {
        $data =  Guru::find($id);
        $data->fill(
            $request->all()
        );
        if (isset($data['passowrd'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            //Do Somting
        }
        
        unset($data['password']);
        $data->save();
        return redirect(route('guru.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        if ($id == $request->id) {
            $data =  Guru::find($id);
            $data->delete();
        }
        return redirect(route('guru.index'));
    }
}
