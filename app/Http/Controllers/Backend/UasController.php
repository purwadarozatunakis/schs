<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Agenda;
use App\Models\Murid;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Guru;
use App\Models\Nilai;
use App\Models\KelasMapel;

class UasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $guru = (\Auth::user()->detail());
        $kode_guru = $guru ? $guru->nip : ''; // todo Stub
        $data = KelasMapel::cutomPagination($kode_guru, 20);
        return view('backend/uas/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $listKelas = Kelas::get()->pluck('nama', 'kode_kelas');
        $listMapel = Mapel::get()->pluck('nama', 'kode_mapel');
        $listMurid = Murid::where('kode_kelas', $request->kode_kelas)->get();
        $listGuru = Guru::get()->pluck('nama', 'nip');
        return view('backend/uas/create', compact('listKelas', 'listMapl', 'listMurid', 'listGuru'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function beriNilaiSave(Request $request)
    {
        $tanggal = $request->tanggal;
        // if(!$tanggal)
        // {
        //     $tanggal = date('Y-m-d') ;   
        // }
        // $uas = Agenda::whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') = '". $tanggal ."'")
        //     ->where('kode_kelas', $request->kode_kelas)
        //     ->where('kode_mapel', $request->kode_mapel)
        //     ->first();


        foreach ($request->nilai as $kd_siswa => $nilai) {
            # code...
            $kode_smester = '1';
            $tahun_ajaran = '2016/2017';
            
            $uas = null;

            if(!$uas){
                $uas = new Nilai();
            }
            $uas->kode_mapel = $request->kelas;
            $uas->kode_smester = $kode_smester;
            $uas->tahun_ajaran = $tahun_ajaran;
            $uas->kode_murid = $kd_siswa;
            $uas->nilai_uts = $nilai ? : 0 ;
            $uas->nilai_uas = 0;
            $uas->keterangan = '';
            $uas->save();
            # code...
        }

        return redirect(route('uas.index', 
        [
            'kode_kelas' => $request->kode_kelas,
            'kode_mapel' => $request->kode_mapel,
            'tanggal' => $request->tanggal,
        ]));
    }

}
