<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::searchAndPaginate(['name'], $request, "search_" , 20);
        return view('backend/users/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data =  new User();
        return view('backend/users/create',compact('data'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSave(Request $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $data['type_user'] = 'ADMIN';
        User::create($data);
        return redirect(route('users.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data =  User::find($id);
        return view('backend/users/edit',compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function editSave(Request $request, $id)
    {
        $data =  User::find($id);
        $data->fill(
            $request->all()
        );
        if(isset($data['passowrd'])){
            $data['password'] = bcrypt($data['password']);
        }else{
            unset($data['password']);
        }
        $data->save();
        return redirect(route('users.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        if($id == $request->id){
            $data =  User::find($id);
            $data->delete();
        }
        return redirect(route('users.index'));
    }

    
}
