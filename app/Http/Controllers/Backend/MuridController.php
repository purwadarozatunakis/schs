<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Murid;
use App\Models\Kelas;
use App\User;
use Excel;

class MuridController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //search_kode_murid
        $data = Murid::searchAndPaginate(['kode_murid','nama'], $request, "search_", 20);
        return view('backend/murid/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data =  new Murid();
        $listKelas = Kelas::get()->pluck('nama', 'kode_kelas');
        return view('backend/murid/create', compact('data', 'listKelas'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUpload()
    {
        $data =  new Murid();
        $listKelas = Kelas::get()->pluck('nama', 'kode_kelas');
        return view('backend/murid/upload', compact('data', 'listKelas'));
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUploadSave(Request $request)
    {

        $filePath = \Storage::putFile('excels', $request->file);
        Excel::load($filePath, function($reader) {
            
            // Dump results and die
            $reader->dd();
        });
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSave(Request $request)
    {
        $data = $request->all();
        $murid = Murid::create($data);
        $user = User::create([
            'name' => $murid->nama,
            'email' => $murid->email,
            'password' => bcrypt($data['password']),
            'type_user' => 'MURID',
        ]);
        return redirect(route('murid.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data =  Murid::find($id);
        $listKelas = Kelas::get()->pluck('nama', 'kode_kelas');
        return view('backend/murid/edit', compact('data', 'listKelas'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function editSave(Request $request, $id)
    {
        $data =  Murid::find($id);
        $data->fill(
            $request->all()
        );
        if (isset($data['passowrd'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            //Do somting
        }
        
        unset($data['password']);
        $data->save();
        return redirect(route('murid.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        if ($id == $request->id) {
            $data =  Murid::find($id);
            $data->delete();
        }
        return redirect(route('murid.index'));
    }
}
