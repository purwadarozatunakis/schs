<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Nilai;
use App\Models\Murid;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\KelasMapel;

class RaporSiswaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $guru = (\Auth::user()->detail());
        $kode_guru = $guru ? $guru->nip : ''; // todo Stub
        $data = KelasMapel::cutomPagination($kode_guru, 20);
        return view('backend/rapot_siswa/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function detailSiswa(Request $request)
    {
        $data = new Nilai();
        $listMurid = Murid::where('kode_kelas', $request->kode_kelas)->get();
        $kelas = Kelas::find($request->kode_kelas);
        $mapel = Mapel::find($request->kode_mapel);
        return view('backend/rapot_siswa/detail_kelas', compact('data','listMurid',  'kelas', 'mapel'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function beriNilaiSiswa(Request $request)
     {
         $rapot_siswa = Nilai::getNilai($request->kode_murid, $request->kode_mapel, 6, '2016/2017');
         $murid = Murid::where('kode_murid', $request->kode_murid)->first();
         return view('backend/rapot_siswa/detail_murid', compact('rapot_siswa','murid'));
     }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSave(Request $request)
    {
        $rapot_siswa = Nilai::doAbsensi($request, $request->tanggal);
        return redirect(route('rapot_siswa.tambah', 
        [
            'kode_kelas' => $request->kode_kelas,
            'kode_mapel' => $request->kode_mapel,
            'tanggal' => $request->tanggal
        ]));
    }

}
