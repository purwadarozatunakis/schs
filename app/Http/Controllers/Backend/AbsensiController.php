<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Absensi;
use App\Models\Murid;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\KelasMapel;

class AbsensiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $guru = (\Auth::user()->detail());
        $kode_guru = $guru ? $guru->nip : ''; // todo Stub
        $data = KelasMapel::cutomPagination($kode_guru, 20);
        return view('backend/absensi/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $kode_kelas, $kode_mapel, $tanggal = null)
    {
        // dd($tanggal);
        $absensi = Absensi::getAbsensi($kode_kelas, $kode_mapel, $tanggal);
        $data = new Absensi();
        $listMurid = Murid::where('kode_kelas', $kode_kelas)->get();
        $kelas = Kelas::find($kode_kelas);
        $mapel = Mapel::find($kode_mapel);
        $listAbsensi = Absensi::getAbsensiKelasMapelExcept($kode_kelas, $kode_mapel, $tanggal);
        return view('backend/absensi/create', compact('data', 'listMurid', 'absensi', 'kelas', 'mapel', 'listAbsensi'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSave(Request $request)
    {
        $absensi = Absensi::doAbsensi($request, $request->tanggal);
        return redirect(route('absensi.tambah', 
        [
            'kode_kelas' => $request->kode_kelas,
            'kode_mapel' => $request->kode_mapel,
            'tanggal' => $request->tanggal
        ]));
    }

}
