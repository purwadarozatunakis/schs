<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Jurusan;
use App\Models\Guru;

class JurusanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Jurusan::searchAndPaginate(['nip','nama'], $request, "search_", 20);
        return view('backend/jurusan/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Jurusan();
        $listGuru = Guru::get()->pluck('nama', 'nip');
        return view('backend/jurusan/create', compact('data', 'listGuru'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSave(Request $request)
    {
        $data = $request->all();
        $jurusan = Jurusan::create($data);
        return redirect(route('jurusan.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data =  Jurusan::find($id);
        $listGuru = Guru::get()->pluck('nama', 'nip');
        return view('backend/jurusan/edit', compact('data', 'listGuru'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function editSave(Request $request, $id)
    {
        $data =  Jurusan::find($id);
        $data->fill(
            $request->all()
        );
        $data->save();
        return redirect(route('jurusan.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        if ($id == $request->id) {
            $data =  Jurusan::find($id);
            $data->delete();
        }
        return redirect(route('jurusan.index'));
    }
}
