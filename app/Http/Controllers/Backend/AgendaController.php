<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Agenda;
use App\Models\Murid;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\KelasMapel;

class AgendaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $guru = (\Auth::user()->detail());
        $kode_guru = $guru ? $guru->nip : ''; // todo Stub
        $data = KelasMapel::cutomPagination($kode_guru, 20);
        return view('backend/agenda/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        $data = Agenda::getTodayAgenda($request->kode_kelas, $request->kode_mapel, $request->tanggal);
        $kelas = Kelas::find($request->kode_kelas);
        $mapel = Mapel::find($request->kode_mapel);
        $listAgenda = Agenda::getAgendaKelasMapelExcept($request->kode_kelas, $request->kode_mapel, $request->tanggal);
        return view('backend/agenda/create', compact('data', 'agenda', 'kelas', 'mapel', 'listAgenda'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSave(Request $request)
    {
        $tanggal = $request->tanggal;
        if(!$tanggal)
        {
            $tanggal = date('Y-m-d') ;   
        }
        $agenda = Agenda::whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') = '". $tanggal ."'")
            ->where('kode_kelas', $request->kode_kelas)
            ->where('kode_mapel', $request->kode_mapel)
            ->first();

        if(!$agenda){
            $agenda = new Agenda();
        }
        $agenda->kode_kelas = $request->kode_kelas;
        $agenda->kode_mapel = $request->kode_mapel;
        $agenda->kode_guru = '182392180312';
        $agenda->catatan = $request->catatan;

        $agenda->save();
        return redirect(route('agenda.tambah', 
        [
            'kode_kelas' => $request->kode_kelas,
            'kode_mapel' => $request->kode_mapel,
            'tanggal' => $request->tanggal,
        ]));
    }

}
