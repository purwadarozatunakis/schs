<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kelas;
use App\Models\Guru;
use App\Models\Jurusan;
use App\Models\Mapel;
use App\Models\KelasMapel;

class KelasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Kelas::searchAndPaginate(['nip','nama'], $request, "search_", 20);
        return view('backend/kelas/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Kelas();
        $listGuru = Guru::get()->pluck('nama', 'nip');
        $listJurusan = Jurusan::get()->pluck('nama', 'kode_jurusan');
        $listMapel = Mapel::get()->pluck('nama', 'kode_mapel');
        return view('backend/kelas/create', compact('data', 'listGuru', 'listJurusan', 'listMapel'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSave(Request $request)
    {
        $data = $request->all();
        $kelas = Kelas::create($data);
        if($request->listMapel){
            foreach ($request->listMapel as $key => $value) {
                if($value != "-"){
                    KelasMapel::create([
                        'kode_kelas' => $request->kode_kelas,
                        'kode_mapel' => $value,
                        'kode_guru' => $request->listGuru[$key]
                    ]);
                }
            }
        }
        return redirect(route('kelas.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data =  Kelas::with('mapel')->find($id);
        $listGuru = Guru::get()->pluck('nama', 'nip');
        $listJurusan = Jurusan::get()->pluck('nama', 'kode_jurusan');
        $listMapel = Mapel::get()->pluck('nama', 'kode_mapel');
        return view('backend/kelas/edit', compact('data', 'listGuru', 'listJurusan', 'listMapel'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function editSave(Request $request, $id)
    {
        $data =  Kelas::find($id);
        $data->fill(
            $request->all()
        );

        $data->save();
        $exsistingKelasMapel = [];
        if($request->listMapel){
            foreach ($request->listMapel as $key => $value) {
                $guru = $request->listGuru[$key];

                $kelasMapel = KelasMapel::where('kode_mapel', $value)
                    ->where('kode_kelas', $request->kode_kelas)->first();

                if($value != "-"){
                    if($kelasMapel){
                        $kelasMapel->kode_guru = $guru;
                        $kelasMapel->save();
                    } else {
                        $kelasMapel = KelasMapel::create([
                            'kode_kelas' => $request->kode_kelas,
                            'kode_mapel' => $value,
                            'kode_guru' => $request->listGuru[$key]
                        ]);
                    }
                    array_push($exsistingKelasMapel, $kelasMapel->id);
                }
            }
        }
        KelasMapel::whereNotIn('id', $exsistingKelasMapel)->delete();
        return redirect(route('kelas.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        if ($id == $request->id) {
            $data =  Kelas::find($id);
            $data->delete();
        }
        return redirect(route('kelas.index'));
    }
}
