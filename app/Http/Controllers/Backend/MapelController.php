<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Mapel;
use App\Models\Guru;
use App\User;

class MapelController extends Controller
{
    protected $jenisMapel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->jenisMapel =  ['Produktif' => 'Produktif', 'Adaptif' => 'Adaptif', 'Normatif' => 'Normatif' ];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Mapel::searchAndPaginate(['nip','nama'], $request, "search_", 20);
        return view('backend/mapel/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Mapel();
        $listGuru = Guru::get()->pluck('nama', 'nip');
        $jenisMapel = $this->jenisMapel;
        return view('backend/mapel/create', compact('data', 'listGuru', 'jenisMapel'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSave(Request $request)
    {
        $data = $request->all();
        $mapel = Mapel::create($data);
        return redirect(route('mapel.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data =  Mapel::find($id);
        $listGuru = Guru::get()->pluck('nama', 'nip');
        $jenisMapel = $this->jenisMapel;
        return view('backend/mapel/edit', compact('data', 'listGuru', 'jenisMapel'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function editSave(Request $request, $id)
    {
        $data =  Mapel::find($id);
        $data->fill(
            $request->all()
        );
        if (isset($data['passowrd'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            //Do Somting
        }
        
        unset($data['password']);
        $data->save();
        return redirect(route('mapel.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        if ($id == $request->id) {
            $data =  Mapel::find($id);
            $data->delete();
        }
        return redirect(route('mapel.index'));
    }
}
