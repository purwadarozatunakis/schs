<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tugas;
use App\Models\Guru;
use App\Models\Murid;
use App\Models\Kelas;
use App\Models\NilaiTugas;
use App\Models\Mapel;

class TugasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = Tugas::searchAndPaginate(['nip','nama'], $request, "search_", 20);
        $listKelas = Kelas::get();
        $listMapel = Mapel::get();
        return view('backend/tugas/index', compact('data'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = new Tugas();
        $listKelas = Kelas::get()->pluck('nama', 'kode_kelas');
        $listMapel = Mapel::get()->pluck('nama', 'kode_mapel');
        $listGuru = Guru::get()->pluck('nama', 'nip');
        return view('backend/tugas/create', compact('data', 'listGuru', 'listKelas', 'listMapel'));
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSave(Request $request)
    {
        $data = $request->all();
        $data['kode_guru'] = '1823021983201';
        $data['kode_tugas'] = $data['kode_kelas'] . '-' . $data['kode_mapel'] . '-' . date('Ymdhis');
        $tugas = Tugas::create($data);
        return redirect(route('tugas.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data =  Tugas::find($id);

        $listKelas = Kelas::get()->pluck('nama', 'kode_kelas');
        $listMapel = Mapel::get()->pluck('nama', 'kode_mapel');
        $listGuru = Guru::get()->pluck('nama', 'nip');
        return view('backend/tugas/edit', compact('data', 'listGuru', 'listKelas', 'listMapel'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function editSave(Request $request, $id)
    {
        $data =  Tugas::find($id);
        $data->fill(
            $request->all()
        );
        $data->save();
        return redirect(route('tugas.index'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function beriNilai(Request $request, $id)
     {
         $data =  Tugas::find($id);
 
         $listKelas = Kelas::get()->pluck('nama', 'kode_kelas');
         $listMapel = Mapel::get()->pluck('nama', 'kode_mapel');
         $listMurid = Murid::where('kode_kelas', $data->kode_kelas)->get();
         $listGuru = Guru::get()->pluck('nama', 'nip');
         return view('backend/tugas/beriNilai', compact('data', 'listGuru', 'listKelas', 'listMapel', 'listMurid'));
     }
 
     /**
      * Show the application dashboard.
      *
      * @return \Illuminate\Http\Response
      */
     public function beriNilaiSave(Request $request, $id)
     {
         $data =  Tugas::find($id);
         NilaiTugas::beriNilai($data, $request);
         return redirect(route('tugas.beriNilai', ['id' => $id]));
     }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        if ($id == $request->id) {
            $data =  Tugas::find($id);
            $data->delete();
        }
        return redirect(route('tugas.index'));
    }
}
