<?php

namespace App\Http\Controllers\Murid;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Absensi;

class MuridTugasController extends Controller
{
    //


    public function index(Request $request)
    {
        $user  = \Auth::user();
        $absensi = [];
        if($user->type_user == 'MURID') {
            $murid = $user->detail();
            $absensi = $murid->getTugas();

        }
        return view('murid/absensi/index', [
            'data' => $absensi
        ]);
    }
}
