<?php

namespace App\Http\Controllers\Murid;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Absensi;

class MuridAbsensiController extends Controller
{
    //


    public function index(Request $request)
    {
        $user  = \Auth::user();
        $absensi = [];
        if($user->type_user == 'MURID') {
            $murid = $user->detail();
            $absensi = $murid->getAbsensi($request->search_kode_smester, $request->search_tahun_ajaran);

        }
        return view('murid/absensi/index', [
            'data' => $absensi
        ]);
    }
}
