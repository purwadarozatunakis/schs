<?php

namespace App\Http\Middleware;

use Closure;

class RacCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $rolename)
    {
        $user = \Auth::user()->hasRole($rolename);
        if ($user) {
            return $next($request);
        } else {
            return redirect('home');
        }
    }
}
