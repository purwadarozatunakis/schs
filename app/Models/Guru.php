<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Guru extends Model
{

    use SearchableTrait;

    protected $table = 't_guru';

    protected $fillable = [
        'nip',
        'nama',
        'email',
        'no_tlp'
    ];

    protected $primaryKey = 'nip';

    public $incrementing = false;
    
    /**
     * Register any application services.
     *
     * @param $searchField
     * @param $request
     * @param $paramElemation
     * @param $paging
     *
     * @return void
     */
    public static function searchAndPaginate($searchFields, $request, $paramElemation, $paging){
        $search = self::whereNotNull("nip");
        foreach ($searchFields as $key => $searchField) {
            if ($request[$paramElemation . $searchField]) {
                $search = $search->where($searchField, 'like', '%'.$request[$paramElemation . $searchField].'%');
            }
            # code...
        }
        return $search->paginate($paging);
    }
}
