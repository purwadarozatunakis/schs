<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class KelasMapel extends Model
{

    use SearchableTrait;

    protected $table = 't_kelas_mapel';

    protected $fillable = [
        'kode_kelas',
        'kode_mapel',
        'kode_guru'
    ];

    
    /**
     * Register any application services.
     *
     * @param $searchField
     * @param $request
     * @param $paramElemation
     * @param $paging
     *
     * @return void
     */
    public static function searchAndPaginate($searchFields, $request, $paramElemation, $paging){
        $search = self::whereNotNull("id");
        foreach ($searchFields as $key => $searchField) {
            if ($request[$paramElemation . $searchField]) {
                $search = $search->where($searchField, 'like', '%'.$request[$paramElemation . $searchField].'%');
            }
            # code...
        }
        return $search->paginate($paging);
    }

    public static function cutomPagination($kode_guru, $paging)
    {
        $search = KelasMapel::paginate($paging);;
        if($kode_guru){
            $search = KelasMapel::where('kode_guru', $kode_guru)->paginate($paging);
        }
        return $search;

    }

    public function kelas()
    {
        return $this->belongsto('App\Models\Kelas', 'kode_kelas', 'kode_kelas');
    }

    public function mapel()
    {
        return $this->belongsto('App\Models\Mapel', 'kode_mapel', 'kode_mapel');
    }

    public function guru()
    {
        return $this->belongsto('App\Models\Guru', 'kode_guru', 'nip');
    }
}
