<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Murid extends Model
{

    use SearchableTrait;

    protected $table = 't_murid';

    protected $fillable = [
        'kode_murid',
        'nama',
        'email',
        'no_tlp',
        'kode_kelas'
    ];

    protected $primaryKey = 'kode_murid';

    public $incrementing = false;
    
    /**
     * Register any application services.
     *
     * @param $searchField
     * @param $request
     * @param $paramElemation
     * @param $paging
     *
     * @return void
     */
    public static function searchAndPaginate($searchFields, $request, $paramElemation, $paging){
        $search = self::whereNotNull("kode_murid");
        foreach ($searchFields as $key => $searchField) {
            if ($request[$paramElemation . $searchField]) {
                $search = $search->where($searchField, 'like', '%'.$request[$paramElemation . $searchField].'%');
            }
            # code...
        }
        return $search->paginate($paging);
    }

    public function absenMuridHariIni($absensi)
    {
        return isset($absensi[$this->kode_murid]) ? $absensi[$this->kode_murid]['data']->keterangan : 'masuk';
        
    }

    public function nilaiTugas($kode_tugas)
    {
        $nilai = NilaiTugas::where('kode_tugas', $kode_tugas)
            ->where('kode_murid', $this->kode_murid)->first();
        return isset($nilai) ? $nilai : new NilaiTugas();
        
    }

    public function getAbsensi($kode_smester, $tahun_ajaran){
        $absensi = Absensi::where('kode_murid', $this->kode_murid)
            ->where('keterangan', '!=', 'masuk')
            ->get();
        return $absensi ? $absensi : [];
    }

}
