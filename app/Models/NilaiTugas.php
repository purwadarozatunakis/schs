<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class NilaiTugas extends Model
{

    use SearchableTrait;

    protected $table = 't_nilai_tugas';

    protected $fillable = [
        'kode_murid',
        'kode_mapel',
        'kode_tugas',
        'kode_smester',
        'tahun_ajaran',
        'nilai',
        'keterangan'
    ];

    protected $primaryKey = 'kode_tugas';

    public $incrementing = false;
    
    /**
     * Register any application services.
     *
     * @param $searchField
     * @param $request
     * @param $paramElemation
     * @param $paging
     *
     * @return void
     */
    public static function searchAndPaginate($searchFields, $request, $paramElemation, $paging){
        $search = self::whereNotNull("kode_tugas");
        foreach ($searchFields as $key => $searchField) {
            if ($request[$paramElemation . $searchField]) {
                $search = $search->where($searchField, 'like', '%'.$request[$paramElemation . $searchField].'%');
            }
            # code...
        }
        return $search->paginate($paging);
    }

    public static function beriNilai($tugas, $request)
    {
        NilaiTugas::where('kode_tugas', $tugas->kode_tugas)->delete();
        foreach ($request->nilai as $key => $nilai) {
            if(!$nilai){
                continue;
            }
            # code...
            NilaiTugas::create([
                'kode_murid' => $key,
                'kode_mapel' => $tugas->kode_mapel,
                'kode_tugas' => $tugas->kode_tugas,
                'kode_smester' => '12',
                'tahun_ajaran' => '2017/2016',
                'nilai' => $nilai,
                'keterangan' => '-'
            ]);
        }
    }

    public function murid()
    {
        return $this->belongsto('App\Models\Murid', 'kode_murid', 'kode_murid');
    }
    public function tugas()
    {
        return $this->belongsto('App\Models\Tugas', 'kode_tugas', 'kode_tugas');
    }
    public function kelas()
    {
        return $this->belongsto('App\Models\Kelas', 'kode_kelas', 'kode_kelas');
    }

    public function mapel()
    {
        return $this->belongsto('App\Models\Mapel', 'kode_mapel', 'kode_mapel');
    }

    public function guru()
    {
        return $this->belongsto('App\Models\Guru', 'kode_guru', 'nip');
    }
}
