<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Tugas extends Model
{

    use SearchableTrait;

    protected $table = 't_tugas';

    protected $fillable = [
        'kode_tugas',
        'kode_kelas',
        'kode_mapel',
        'kode_guru',
        'jenis_tugas',
        'tanggal_pengumpulan',
        'soal_tugas'
    ];

    protected $primaryKey = 'kode_tugas';

    public $incrementing = false;
    
    /**
     * Register any application services.
     *
     * @param $searchField
     * @param $request
     * @param $paramElemation
     * @param $paging
     *
     * @return void
     */
    public static function searchAndPaginate($searchFields, $request, $paramElemation, $paging){
        $search = self::whereNotNull("kode_tugas");
        foreach ($searchFields as $key => $searchField) {
            if ($request[$paramElemation . $searchField]) {
                $search = $search->where($searchField, 'like', '%'.$request[$paramElemation . $searchField].'%');
            }
            # code...
        }
        return $search->paginate($paging);
    }

    public function kelas()
    {
        return $this->belongsto('App\Models\Kelas', 'kode_kelas', 'kode_kelas');
    }

    public function mapel()
    {
        return $this->belongsto('App\Models\Mapel', 'kode_mapel', 'kode_mapel');
    }

    public function guru()
    {
        return $this->belongsto('App\Models\Guru', 'kode_guru', 'nip');
    }
}
