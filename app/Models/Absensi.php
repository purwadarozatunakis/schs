<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;
use DB;

class Absensi extends Model
{

    use SearchableTrait;

    protected $table = 't_absensi';

    protected $fillable = [
        'kode_murid',
        'kode_guru',
        'kode_kelas',
        'kode_mapel',
        'keterangan',
        'keterangan_tambahan',
        'kode_smester',
        'tahun_ajaran'
    ];

    
    /**
     * Register any application services.
     *
     * @param $searchField
     * @param $request
     * @param $paramElemation
     * @param $paging
     *
     * @return void
     */
    public static function searchAndPaginate($searchFields, $request, $paramElemation, $paging){
        $search = self::whereNotNull("id");
        foreach ($searchFields as $key => $searchField) {
            if ($request[$paramElemation . $searchField]) {
                $search = $search->where($searchField, 'like', '%'.$request[$paramElemation . $searchField].'%');
            }
            # code...
        }
        return $search->paginate($paging);
    }

    public static function getAbsensi($kelas, $mapel, $tanggal = null)
    {
        if(!$tanggal)
        {
            $tanggal = date('Y-m-d') ;   
        }
        $data = Absensi::whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') = '". $tanggal ."'")
            ->where('kode_kelas', $kelas)
            ->where('kode_mapel', $mapel)->get();
        $absensiData = [];
        foreach ($data as $key => $value) {
            $absensiData[$value->kode_murid]['data'] = $value;
            # code...
        }
        return $absensiData;
    }
    public static function doAbsensi($data, $tanggal = null)
    {
        if(!$tanggal)
        {
            $tanggal = date('Y-m-d') ;   
        }
        $kelas = $data->kelas;
        $mapel = $data->mapel;
        $listMurid = $data->murid;
        $guru = $data->guru;
        $keterangan = $data->keterangan;
        $keterangan_tambahan = $data->keterangan_tambahan;

        foreach ($listMurid as $key => $murid) {

            $absen = Absensi::whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') = '". $tanggal ."'")
            ->where('kode_kelas', $kelas)
            ->where('kode_mapel', $mapel)
            ->where('kode_murid', $murid)
                ->first();
            if(!$absen){
                $absen = new Absensi();
            }
            $absen->fill([
                'kode_murid' => $murid,
                'kode_guru' => "9128309128",
                'kode_mapel' => $mapel,
                'kode_kelas' => $kelas,
                'keterangan' => $keterangan[$murid],
                'keterangan_tambahan' => $keterangan_tambahan[$murid]
            ]);
            $absen->save();
            # code...
        }
    }
    
    public function formatedCreatedDate($format)
    {
        # code...
        return date_format($this->created_at,$format);
    }

    public static function getAbsensiKelasMapelExcept($kelas, $mapel, $tanggal = null)
    {
        if(!$tanggal)
        {
            $tanggal = date('Y-m-d') ;   
        }
        $data = Absensi::selectRaw("DATE_FORMAT(created_at, '%Y-%m-%d') AS created_at")->whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') != '". $tanggal ."'")
            ->where('kode_kelas', $kelas)
            ->where('kode_mapel', $mapel)
            ->groupBy(DB::raw("(DATE_FORMAT(created_at, '%Y-%m-%d'))"))
            ->get();
        // dd($data->toSql());
        return $data;
    }

    public function kelas()
    {
        return $this->belongsto('App\Models\Kelas', 'kode_kelas', 'kode_kelas');
    }
    public function murid()
    {
        return $this->belongsto('App\Models\Murid', 'kode_murid', 'kode_murid');
    }

    public function mapel()
    {
        return $this->belongsto('App\Models\Mapel', 'kode_mapel', 'kode_mapel');
    }

}
