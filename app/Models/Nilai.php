<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Nilai extends Model
{

    use SearchableTrait;

    protected $table = 't_nilai';

    protected $fillable = [
        'kode_murid',
        'kode_mapel',
        'kode_smester',
        'tahun_ajaran',
        'nilai_uts',
        'nilai_uas',
        'keterangan'
    ];

    protected $primaryKey = 'id';

    public $incrementing = false;
    
    /**
     * Register any application services.
     *
     * @param $searchField
     * @param $request
     * @param $paramElemation
     * @param $paging
     *
     * @return void
     */
    public static function searchAndPaginate($searchFields, $request, $paramElemation, $paging){
        $search = self::whereNotNull("kode_tugas");
        foreach ($searchFields as $key => $searchField) {
            if ($request[$paramElemation . $searchField]) {
                $search = $search->where($searchField, 'like', '%'.$request[$paramElemation . $searchField].'%');
            }
            # code...
        }
        return $search->paginate($paging);
    }

    public static function getNilai($kode_murid, $kode_mapel, $kode_smester, $tahun_ajaran)
    {
        $kode_smester = '1';
        $tahun_ajaran = '2016/2017';
        $data = Nilai::where('kode_murid', $kode_murid)
            ->where('kode_smester', $kode_smester)
            ->where('kode_mapel', $kode_mapel)
            ->where('tahun_ajaran', $tahun_ajaran)->first();
        if(!$data){
            $data = new Nilai();
            $data->kode_smester = 1;
            $data->tahun_ajaran = '2016/2017';
        }
        return $data;
    }

    public static function beriNilai($tugas, $request)
    {
        foreach ($request->nilai_uts as $key => $nilai) {
            # code...
            $kode_smester = '1';
            $tahun_ajaran = '2016/2017';


            $nilai = NilaiTugas::where('kode_tugas', $tugas->kode_mapel)
                ->where('kode_murid', $key)
                ->where('kode_smester', $kode_smester)
                ->where('tahun_ajaran', $tahun_ajaran)
                ->first();
                
            if(!$nilai)
            {
                $nilai = new Nilai();
            }

            
            $nilai->kode_murid=  $key;
            $nilai->kode_mapel=  $tugas->kode_mapel;
            $nilai->kode_smester=  $kode_smester;
            $nilai->tahun_ajaran=  $tahun_ajaran;
            $nilai->nilai_uts=  $nilai;
            $nilai->nilai_uas=  $request->nilai_uas[$key];
            $nilai->keterangan=  '-';

            $nilai->save();
        }
    }

    public function murid()
    {
        return $this->belongsto('App\Models\Murid', 'kode_murid', 'kode_murid');
    }
    public function tugas()
    {
        return $this->belongsto('App\Models\Tugas', 'kode_tugas', 'kode_tugas');
    }
    public function kelas()
    {
        return $this->belongsto('App\Models\Kelas', 'kode_kelas', 'kode_kelas');
    }

    public function mapel()
    {
        return $this->belongsto('App\Models\Mapel', 'kode_mapel', 'kode_mapel');
    }

    public function guru()
    {
        return $this->belongsto('App\Models\Guru', 'kode_guru', 'nip');
    }
}
