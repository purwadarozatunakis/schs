<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;
use App\Models\KelasMapel;

use App\Models\Agenda;

class Kelas extends Model
{

    use SearchableTrait;

    protected $table = 't_kelas';

    protected $fillable = [
        'kode_kelas',
        'nama',
        'wali_kelas',
        'kode_jurusan',
        'tingkat',
    ];

    protected $primaryKey = 'kode_kelas';

    public $incrementing = false;
    
    
    /**
     * Register any application services.
     *
     * @param $searchField
     * @param $request
     * @param $paramElemation
     * @param $paging
     *
     * @return void
     */
    public static function searchAndPaginate($searchFields, $request, $paramElemation, $paging){
        $search = self::whereNotNull("kode_kelas");
        foreach ($searchFields as $key => $searchField) {
            if ($request[$paramElemation . $searchField]) {
                $search = $search->where($searchField, 'like', '%'.$request[$paramElemation . $searchField].'%');
            }
            # code...
        }
        return $search->paginate($paging);
    }

    public function mapel()
    {
        return $this->hasMany('App\Models\KelasMapel', 'kode_kelas', 'kode_kelas');
    }
    public function agenda($kode_mapel)
    {
        return Agenda::where('kode_kelas', $this->kode_kelas)
            ->where('kode_mapel', $kode_mapel)->first() ? : new Agenda();
    }
}
