<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Agenda extends Model
{

    use SearchableTrait;

    protected $table = 't_agenda';

    protected $fillable = [
        'kode_guru',
        'kode_kelas',
        'kode_mapel',
        'catatan'
    ];

    
    /**
     * Register any application services.
     *
     * @param $searchField
     * @param $request
     * @param $paramElemation
     * @param $paging
     *
     * @return void
     */
    public static function searchAndPaginate($searchFields, $request, $paramElemation, $paging){
        $search = self::whereNotNull("id");
        foreach ($searchFields as $key => $searchField) {
            if ($request[$paramElemation . $searchField]) {
                $search = $search->where($searchField, 'like', '%'.$request[$paramElemation . $searchField].'%');
            }
            # code...
        }
        return $search->paginate($paging);
    }

    public static function getTodayAgenda($kelas, $mapel, $tanggal = null)
    {
        if(!$tanggal)
        {
            $tanggal = date('Y-m-d') ;   
        }
        $data = Agenda::whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') = '". $tanggal ."'")
            ->where('kode_kelas', $kelas)
            ->where('kode_mapel', $mapel)->first();
        if(!$data){
            $data=  new Agenda();
        }
        return $data;
    }

    public static function getAgendaKelasMapelExcept($kelas, $mapel, $tanggal = null)
    {
        if(!$tanggal)
        {
            $tanggal = date('Y-m-d') ;   
        }
        $data = Agenda::select(['created_at'])->whereRaw("DATE_FORMAT(created_at, '%Y-%m-%d') != '". $tanggal ."'")
            ->where('kode_kelas', $kelas)
            ->where('kode_mapel', $mapel)
            ->groupBy('created_at')
            ->get();
        return $data;
    }

    public function formatedCreatedDate($format)
    {
        # code...
        return date_format($this->created_at,$format);
    }
}
