<?php

namespace App\Traits;


trait SearchableTrait 
{
    public static function searchAndPaginate($searchFields , $request , $paramElemation, $paging){
        $search = self::whereNotNull('id');
        foreach ($searchFields as $key => $searchField) {
            if($request[$paramElemation . $searchField]){
                $search = $search->where($searchField, 'like' , '%'.$request[$paramElemation . $searchField].'%');
            }
            # code...
        }
        return $search->paginate($paging);

    }
}