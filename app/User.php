<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Traits\SearchableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SearchableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type_user'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function detail()
    {
        if($this->type_user == 'GURU') {
            return \App\Models\Guru::where('email', $this->email)->first();
        } else if ($this->type_user == 'MURID') {
            return \App\Models\Murid::where('email', $this->email)->first();
        }
        return null;
    }

    public function hasRole($role)
    {
        if ($this->type_user == 'ADMIN') {
            return true;
        } else {
            return ($this->type_user == $role);
        }
    }
}
